export default {
  tooltip: {
    trigger: 'axis'
  },
  color: ['#44C89A', '#FEC870', '#36A4F1', '#FF786E'],
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: []
  }],
  grid: {
    top: 30,
    bottom: 30
  },
  yAxis: [{
    type: 'value'
  }],
  series: [{
    type: 'line',
    data: []
  }]
}
