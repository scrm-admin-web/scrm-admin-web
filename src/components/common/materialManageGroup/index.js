import materialGroup from './components/materialGroup'
// import quickGroup from './components/quickGroup'
import mManageGroupAdd from './components/mManageGroupAdd'
import materialDialog from './components/materialDialog'
// import quickDialog from './components/quickDialog'

export {
    materialGroup, // 素材管理--左侧分组
    mManageGroupAdd, // 素材管理--左侧新增，编辑
    materialDialog, // 素材管理--弹窗
    // quickDialog, //素材管理-快捷语选择
    // quickGroup
}