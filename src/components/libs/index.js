import VmaDialog from './vmaDialog/index.vue'
import H5View from './h5View'
import BtnGroup from './btnGroup'

export {
  VmaDialog,
  H5View,
  BtnGroup
}
