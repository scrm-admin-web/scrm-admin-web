import Expression from './expression'

const expressMap = {}
Expression.EXPRESS_LIST.forEach(v => {
  v.list.forEach(e => {
    // 需要映射的表情缓存到map
    // 无需映射的通常为unicode已支持的emoji
    if (e.mapping) {
      expressMap[e.title || e.icon] = e
    }
  })
})

// 转图标
const parseToIcon = (title) => {
  let icon = ''
  if (expressMap[title]) {
    icon = expressMap[title].icon
  }
  return `<i class="icon-wechat-express ${icon}" express="${title}"></i>`
}

const REG_EXPRESSION_TITLE = /(\[.+?\])/ig
// const REG_EXPRESSION_LINE = /(\r\n)|↵/ig
const REG_EXPRESSION_LINE = /(\r\n|\n|\r)/gm
// 将包含表情的文本转换成html
export const parseHTML = (content) => {
  if (content && typeof content === 'string') {
    content = content.replace(REG_EXPRESSION_TITLE, title => parseToIcon(title))
      .replace(REG_EXPRESSION_LINE, '<br />')
  }
  return content
}

const REG_EXPRESSION_ICON = /(<i[^>]+express=["|']([^>]+)["|'][^>]*>[^<]*<\/i>)/gi
// 将包含表情的html转换成文本
export const parseText = (html) => {
  if (html && typeof html === 'string') {
    html = html.replace(REG_EXPRESSION_ICON, '$2')
  }
  return html
}
