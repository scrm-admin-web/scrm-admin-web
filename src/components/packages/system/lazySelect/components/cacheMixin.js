export default {
  data() {
    return {
      cacheMap: {}
    }
  },
  methods: {
    getKey() {
      throw new Error('请重写该方法提供唯一key')
    },
    getDataFnOrDataList() {
      const key = this.getKey()
      if (this.cacheMap[key]) {
        return Promise.resolve(this.cacheMap[key])
      } else {
        return this.getData().then(data => {
          this.cacheMap[key] = data
          return data
        })
      }
    }
  }
}
