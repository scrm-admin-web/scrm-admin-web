import DemoLazySelect from './components/demo'
import staffDepartmentSelect from './components/staffDepartment'
import groupingSelect from './components/friendsGrouping'
import groupGrouping from './components/groupGrouping'
import deviceGroupingSelect from './components/deviceGrouping'
import equipmentEmployees from './components/equipmentEmployees'
import roleSelect from './components/role'
import affiliatedShopSelect from './components/affiliatedShop'
import dManageDepartment from './components/dManageDepartment'
import dManageDepartmentStaff from './components/dManageDepartmentStaff'
import departmentStaff from './components/departmentStaff'
import importOrderDepartmentStaff from './components/importOrderDepartmentStaff'
import wechatAccount from './components/wechatAccount'
import wechatAccountNew from './components/wechatAccountNew'
import wechatAccountStatus from './components/wechatAccountStatus'
import wechatAccountStatusNew from './components/wechatAccountStatusNew'
import wechatLabel from './components/wechatLabel'
import ownCustomer from './components/ownCustomer'
import WechatListLazySelect from './components/wechatList'
import mManageDepartment from './components/mManageDepartment'
import mManageDepartmentSec from './components/mManageDepartmentSec'
import shopCategory from './components/shopCategory'
import personNumWechatGroups from './components/personNumWechatGroups'
import personNumFriendGroups from './components/personNumFriendGroups'
import shopList from './components/shopList'
import payMethod from './components/payMethod'
import senitiveGroup from './components/senitiveGroup'

export {
  DemoLazySelect,
  staffDepartmentSelect, // 员工所在部门
  groupingSelect, // 所属分组
  groupGrouping, // 群分组
  deviceGroupingSelect, // 设备分组
  equipmentEmployees, // 设备所属员工
  roleSelect, // 角色
  affiliatedShopSelect, // 所属店铺
  dManageDepartment, // 接收员工部门
  dManageDepartmentStaff, // 接收员工部门--下的员工
  departmentStaff,
  importOrderDepartmentStaff, // 导入订单获取员工
  wechatAccount, // 个人号
  wechatAccountNew, // 个人好（新）
  wechatAccountStatus, // 个人号添加--disable
  wechatAccountStatusNew, // 个人号添加--disable(新的)
  wechatLabel, // 加好友后打标签
  ownCustomer, // 所属客服
  mManageDepartment, // 素材管理，部门获取--获取树，只显示一级
  mManageDepartmentSec, // 素材管理，部门获取--获取二级
  shopCategory, // 店铺类目
  personNumWechatGroups, // 个人号管理--微信群查看--分组管理
  personNumFriendGroups, // 个人号管理--好友列表--分组管理
  shopList, // 营销平台--微跳转
  payMethod, // 支付方式
  senitiveGroup // 分控--敏感词设置--敏感词分组
}

export {
  // 个人号下拉框
  WechatListLazySelect
}
