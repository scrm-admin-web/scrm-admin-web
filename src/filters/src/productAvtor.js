import {
  defaultProductImg
} from '@/config'

// 商品默认图片
export default value => {
  return value || defaultProductImg
}
