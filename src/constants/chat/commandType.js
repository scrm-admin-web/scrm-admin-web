import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 获取朋友圈
  CIRCLE: 'List',
  // 根据朋友圈id取朋友圈
  CIRCLE_GET: 'Get',
  // 朋友圈回复
  CIRCLE_REPLY: 'Reply',
  // 点赞
  CIRCLE_LIKE: 'Like',
  // 朋友圈评论
  CIRCLE_COMMENT: 'Comment'
})
