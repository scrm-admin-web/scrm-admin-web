import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 文本消息
  VMA_TEXT_ELEM: 'VmaTextElem',
  // 语音消息
  VMA_SOUND_ELEM: 'VmaSoundElem',
  // 图片
  VMA_IMAGE_ELEM: 'VmaImageElem',
  // 视频
  VMA_VIDEO_ELEM: 'VmaVideoElem',
  // 小程序
  VMA_MINI_PROGRAM_ELEM: 'VmaMiniProgramElem',
  // 文件
  VMA_FILE_ELEM: 'VmaFileElem',
  // 系统消息
  VMA_SYSTEM_ELEM: 'VmaSystemElem',
  // 微信分享名片
  WX_BUSINESS_CARD_ELEM: 'WxBusinessCardElem',
  // 位置
  WX_LOCATION_ELEM: 'WxLocationElem',
  // 转账
  WX_TRANSFER_ELEM: 'WxTransferElem',
  // 微信红包
  WX_RED_PACKET_ELEM: 'WxRedPacketElem',
  // 微信共享位置消息
  WX_SHARE_LOCATION_ELEM: 'WxShareLocationElem',
  // 撤销消息
  VMA_CANCEL_ELEM: 'VmaCancelElem',
  // 微客服收到客户消息，发的已读回执
  VMA_READ_ELEM: 'VmaReadElem',
  // 朋友圈
  VMA_MOMENTS_ELEM: 'VmaMomentsElem',
  // 消息群发
  VMA_GROUP_SEND_ELEM: 'VmaGroupSendElem',
  // 手机上删除消息
  VMA_DELETE_ELEM: 'VmaDeleteElem',
  // 图文链接
  VMA_IMGTXTLINK_ELEM: 'VmaImgtxtlinkElem'
})
