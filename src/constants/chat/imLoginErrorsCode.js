import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // IM套餐未续费
  WAIT_RENEWAL_AGAIN: 60020
})
