import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 集中对话
  CONCENTRATED_DIALOGUE: 'concentratedDialogue',
  // 隐藏空分组
  HIDE_EMPTY_GROUP: 'hideEmptyGroup',
  // 自动回复
  REPLY_MSG_AUTO: 'replyMsgAuto'
})
