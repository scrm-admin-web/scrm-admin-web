import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 接收
  RECEIVED: 1,
  // 发送
  SEND: 2
})
