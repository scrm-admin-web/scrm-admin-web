import {
  Constant
} from 'vma-vue-assist'

// 处理状态
const PROCESS_STATE = new Constant([{
  value: 1,
  label: '已处理'
}, {
  value: 0,
  label: '待处理'
}])

export default PROCESS_STATE.valueList()
