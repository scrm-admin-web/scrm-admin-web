import {
  Constant
} from 'vma-vue-assist'

// 进群方式
const GROUP_STATUS = new Constant([{
  value: 1,
  label: '正常'
}, {
  value: 2,
  label: '退出'
}])

export default GROUP_STATUS.valueList()
