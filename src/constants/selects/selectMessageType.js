import {
  Constant
} from 'vma-vue-assist'

const SELECT_MESSAGE_TYPE = new Constant([{
  value: 1,
  label: '文本'
}, {
  value: 2,
  label: '图片'
}, {
  value: 3,
  label: '语音'
}, {
  value: 4,
  label: '视频'
}])

export default SELECT_MESSAGE_TYPE.valueList()
