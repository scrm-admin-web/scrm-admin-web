import {
  Constant
} from 'vma-vue-assist'

// 行为管理
const BEHAVIOR_MANAGE = new Constant([{
  value: 1,
  label: '消息敏感词'
}, {
  value: 2,
  label: '发送公众号名片/个人号名片'
}, {
  value: 3,
  label: '发送链接分享'
}, {
  value: 4,
  label: '发送小程序'
}, {
  value: 5,
  label: '发送小视频'
}, {
  value: 6,
  label: '防骚扰'
}, {
  value: 7,
  label: '群名称锁定禁止修改'
}])

export default BEHAVIOR_MANAGE.valueList()
