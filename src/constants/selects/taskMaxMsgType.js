import {
  Constant
} from 'vma-vue-assist'

const TSAKMAXMSGTYPE = new Constant([{
    code: '01',
    name: '车险销售任务'
  },
  {
    code: '02',
    name: '非车险销售任务'
  },
  {
    code: '03',
    name: '服务任务'
  },
  {
    code: '04',
    name: '消息通知'
  },
  {
    code: '05',
    name: '其他'
  }
])

export default TSAKMAXMSGTYPE.valueList()
