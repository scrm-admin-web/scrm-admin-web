import {
  Constant
} from 'vma-vue-assist'

// 违规行为
const IRREGULARITIES = new Constant([{
  value: 1,
  label: '群聊内容触发敏感词'
}, {
  value: 2,
  label: '群内发送名片'
}, {
  value: 3,
  label: '群内发送链接'
}, {
  value: 4,
  label: '群内发送小程序'
}, {
  value: 5,
  label: '群内发送小视频'
}, {
  value: 6,
  label: '防骚扰'
}, {
  value: 7,
  label: '群名锁定禁止修改'
}, {
  value: 8,
  label: '已加入黑名单，系统自动踢人'
}])

export default IRREGULARITIES.valueList()
