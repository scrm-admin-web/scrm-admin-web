// 图表转表格
export const optionToContent = opts => {
  let axisData = opts.xAxis[0].data
  let legendData = opts.legend[0].data
  let seriesData = opts.series
  let table = `<table style="width:100%;text-align:center;font-size:1rem;color:#333;">
                <thead>
                  <tr>
                    <th>x轴</th>`
  legendData.forEach(t => {
    table += `<th>${t}</th>`
  })
  table += '</tr></thead><tbody>'
  axisData.forEach((t, i) => {
    table += `<tr><td>${t}</td>`
    seriesData.forEach((v, j) => {
      table += `<td>${v.data[i]}</td>`
    })
    table += '</tr>'
  })
  table += `</tbody></table>`
  return table
}
