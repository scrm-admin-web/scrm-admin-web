/**
 * 解决float乘法的精度丢失
 * @param  {[type]} f     进行乘法后精度丢失的值
 * @param  {[type]} digit 保留的小数点位数
 */
const formatFloat = function (f, digit = 2) {
  var m = Math.pow(10, digit)
  return Math.round(f * m, 10) / m
}
export default formatFloat
