import {
  MessageBox
} from 'element-ui'

export const resolveRouterComponent = promise => {
  return promise.catch(e => {
    MessageBox.confirm('页面加载失败，点击确定或刷新页面重新加载', '系统提示', {
      type: 'warning',
      showClose: false,
      showCancelButton: false,
      callback(action) {
        if (action === 'confirm') {
          window.location.reload()
        }
      }
    })
  })
}
