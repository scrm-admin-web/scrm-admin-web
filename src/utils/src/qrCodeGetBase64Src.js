import QRCode from 'qrcodejs2'
import Logger from '../logger'

export default async (option) => {
  let div = document.createElement('div')
  let qrcode = new QRCode(div, option)
  let base64Src = await new Promise((resolve) => {
    qrcode._oDrawing._elImage.onload = function() {
      Logger.log(this.src)
      resolve(this.src)
    }
  })
  qrcode = null
  div = null
  return base64Src
}
