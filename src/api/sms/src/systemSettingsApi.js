import axios from 'axios'
const API_MODULE_V = `/super/v1.0`
const API_MODULE_S = `/super/v2.0`
const removeEmpty = true
export default {
  /**
   * 获取系统配置信息
   */
  getSystemSettingsInfo() {
    return axios({
      url: `${API_MODULE_V}/systemconfig/update_config`,
      method: 'GET'
    })
  },
  /**
   * 系统配置修改
   * @param {*} params
   */
  updateSystemSettings(params) {
    return axios({
      url: `${API_MODULE_V}/systemconfig/`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 超级后台app版本管理
   * @param {*} params
   */
  appVersion(params) {
    return axios({
      url: `${API_MODULE_V}/systemconfig/app_version`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 下拉框app类型
   * @param {*} params
   */
  appType(params) {
    return axios({
      url: `${API_MODULE_V}/systemconfig/app_type`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 添加app版本管理
   * @param {*} params
   */
  addAppVersion(params) {
    return axios({
      url: `${API_MODULE_V}/systemconfig/add_app_version`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 套餐管理--获取套餐列表
   * @param {*} params
   */
  getPackageRecords(params) {
    return axios({
      url: `${API_MODULE_S}/package`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 套餐管理--删除
   * @param {*} params
   */
  delPackage(params) {
    return axios({
      url: `${API_MODULE_S}/package/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 套餐管理--套餐上架、下架
   * @param {*} params
   */
  changePackageStatus(params) {
    return axios({
      url: `${API_MODULE_S}/package/updateStatus`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 套餐管理--新增
   * @param {*} params
   */
  addPackageRecord(params) {
    return axios({
      url: `${API_MODULE_S}/package`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  },
  /**
   * 套餐管理--编辑
   * @param {*} params
   */
  editPackageRecord(params) {
    return axios({
      url: `${API_MODULE_S}/package`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 域名配置--获取域名列表
   * @param {*} params
   */
  getDomainRecord(params) {
    return axios({
      url: `${API_MODULE_S}/domain`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 域名配置--域名启用,禁用
   * @param {*} params
   */
  toggleDomainStatus(params) {
    return axios({
      url: `${API_MODULE_S}/domain/updateStatus`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 域名配置--新增域名
   * @param {*} params
   */
  addDomainReq(params) {
    return axios({
      url: `${API_MODULE_S}/domain/${params}`,
      method: 'POST'
    })
  },
  /**
   * app版本管理--列表获取
   * @param {*} params
   */
  getAppVersionRecords(params) {
    return axios({
      url: `${API_MODULE_S}/systemconfig/app_version`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * app版本管理--获取app类型
   * @param {*} params
   */
  getAppVersionType(params) {
    return axios({
      url: `${API_MODULE_S}/systemconfig/app_type`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * app版本管理--新增app版本
   * @param {*} params
   */
  addAppVersionReq(params) {
    return axios({
      url: `${API_MODULE_S}/systemconfig/add_app_version`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  }
}
