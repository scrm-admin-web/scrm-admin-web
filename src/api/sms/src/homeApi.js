import axios from 'axios'

export default {
  /**
   * 企业数曲线图
   * @returns
   */
  getEnterpriseMessage(params) {
    return axios({
      url: '/super/v1.0/index/home/enterprise/message',
      method: 'GET',
      params
    })
  },
  /**
   * 设备数据
   * @returns
   */
  getEquipmentMessage() {
    return axios({
      url: '/super/v1.0/index/home/equipment/message',
      method: 'GET'
    })
  },
  /**
   * 授权店铺
   * @returns
   */
  getShopAccredit(params) {
    return axios({
      url: '/super/v1.0/index/home/shop/accredit',
      method: 'GET',
      params
    })
  },
  /**
   * 个人号数据
   * @returns
   */
  getWeChatMessage() {
    return axios({
      url: '/super/v1.0/index/home/wet_chat/message',
      method: 'GET'
    })
  },
  /**
   * 个人号总数
   * @returns
   */
  getWeChatTotal() {
    return axios({
      url: '/super/v1.0/index/home/wet_chat/total',
      method: 'GET',
      loading: false
    })
  }
}
