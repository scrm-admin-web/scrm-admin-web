import axios from 'axios'
const API_MODULE_V = `/super/v1.0`

export default {
  // 客户管理-贴牌商列表
  /**
   * 获取贴牌商列表
   * @param {*} params
   */
  getOemList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 新增贴牌商
   */
  newOem(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 编辑贴牌商
   */
  editOem(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 贴牌商详情-编辑用
   */
  detailOem(id) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/edit/${id}`,
      method: 'GET'
    })
  },
  /**
   * 贴牌商详情
   */
  getOemDetail(id) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/${id}`,
      method: 'GET'
    })
  },
  /**
   * 代理商列表
   */
  getProxyList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/proxy`,
      method: 'GET',
      params: params,
      removeEmpty: true
    })
  },
  /**
   * 企业列表
   */
  getEnterpriseList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/enterprise`,
      method: 'GET',
      params: params,
      removeEmpty: true
    })
  },
  /**
   * 设备列表
   */
  getEuqList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/euq`,
      method: 'GET',
      params: params,
      removeEmpty: true
    })
  },
  /**
   * 新增设备
   */
  newEuq(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/equ`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 店铺列表
   */
  getShopList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/shop`,
      method: 'GET',
      params: params,
      removeEmpty: true
    })
  },
  /**
   * 新增店铺
   */
  newShop(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/shop`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 个人号列表
   */
  getAccountList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/account`,
      method: 'GET',
      params: params,
      removeEmpty: true
    })
  },
  /**
   * 新增个人号
   */
  newAccount(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/account`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 新增账号
   */
  newEnter(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/enter`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 通知列表
   */
  getNoticeList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/notice`,
      method: 'GET',
      params: params,
      removeEmpty: true
    })
  },
  /**
   * 禁用/启用
   */
  setIsable(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/isable`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 重置密码
   */
  resetPassword(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/reset`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 登录域名
   */
  checkLoginIdenty(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/check`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 新增群
   *
   * @param {*} params
   * @returns
   */
  groupAdd(params) {
    return axios({
      url: `${API_MODULE_V}/customer/oem/group`,
      method: 'POST',
      data: params
    })
  },
  // 新增手机型号
  addphone(params){
    return axios({
      url: `${API_MODULE_V}/customer/oem/addInstallPhone`,
      method: 'POST',
      data: params
    })
  },
  // 获取手机型号
  PhoneList(params){
    return axios({
      url: `${API_MODULE_V}/customer/oem/installPhoneList`,
      method: 'GET',
      data: params
    })
  }
}
