import axios from 'axios'
const API_MODULE = `/manager/v1.0/quick`

export default {
  // AI模块获取公司库分组
  getAllComPanyGroup(params) {
    return axios({
      url: `${API_MODULE}/reply/allComPanygroup`,
      method: 'GET',
      params,
      responseType: 'json',
      loading: false
    })
  },

  // AI模块获取公司库下分组数据
  getNoPageCompany(params) {
    return axios({
      url: `${API_MODULE}/reply/nopageCompany`,
      method: 'GET',
      params,
      responseType: 'json',
      loading: false
    })
  },

  /**
   * 获取获取私人快捷回复分组列表
   */
  getProprietaryQuickReplyGorupList(params) {
    return axios({
      url: `${API_MODULE}/reply/groupim`,
      method: 'GET',
      params,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 获取获取公共快捷回复分组列表
   */
  getPublicQuickReplyGorupList(params) {
    return axios({
      url: `${API_MODULE}/reply/allgroup`,
      method: 'GET',
      params,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 获取获取快捷回复分组列表(新)
   */
  getQuickReplyGorupListIm(params) {
    return axios({
      url: `${API_MODULE}/reply/selim`,
      method: 'GET',
      params,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 获取快捷回复分组下的列表
   */
  getProprietaryQuickReplyDetail(params) {
    return axios({
      url: `${API_MODULE}/reply/nopage`,
      method: 'GET',
      params,
      responseType: 'json'
    })
  },
  // 公司库查询接口
  getCompanySearch(params) {
    return axios({
      url: `${API_MODULE}/reply/selai`,
      method: 'GET',
      params,
      responseType: 'json'
    })
  }
}
