import axios from 'axios'

const API_MODULE = `/manager/v1.0/custom_service`
const removeEmpty = true

export default {
  /**
   * 获取微客服拥有的个人号列表
   */
  getPersonalAccountList() {
    return axios({
      url: `${API_MODULE}/wx_account`,
      method: 'GET',
      responseType: 'json'
    })
  },
  /**
   * 获取微客服拥有的个人号列表（新）
   */
  getPersonalAccountListNew() {
    return axios({
      url: '/manager/v2.0/wechat/account/data/wx_account',
      method: 'GET',
      responseType: 'json'
    })
  },
  /**
   * 获取微客服拥有的个人号列表
   */
  checkUpMineCustom(data) {
    return axios({
      url: `${API_MODULE}/wechat_user/mine`,
      method: 'POST',
      data,
      responseType: 'json'
    })
  },
  /**
   * 收藏消息
   * @param  {[type]} data [description]
   * @return {[type]}        [description]
   */
  collectMessage(data) {
    return axios({
      url: `${API_MODULE}/collection`,
      method: 'POST',
      responseType: 'json',
      data
    })
  },
  /**
   * 根据id获取最近聊天人/群的数据
   * @param  {[type]} data [description]
   * @return {[type]}        [description]
   */
  getPersonalAccountRecentChatsInfoListByIds(data) {
    return axios({
      url: `${API_MODULE}/lately_chat`,
      method: 'POST',
      responseType: 'json',
      data,
      loading: false
    })
  },
  /**
   * 根据id获取最近聊天人/群的数据,集中对话
   * @param  {[type]} data [description]
   * @return {[type]}        [description]
   */
  getRecentChatsInfoListByIdsNoAccount(data) {
    return axios({
      url: `${API_MODULE}/lately_chat_no_account`,
      method: 'POST',
      responseType: 'json',
      data,
      loading: false
    })
  },
  /**
   * 给群聊添加/修改备注
   * @param {[type]} data [description]
   */
  setChatGroupRemark(data) {
    return axios({
      url: `${API_MODULE}/change/group_remark`,
      method: 'PUT',
      responseType: 'json',
      data,
      removeEmpty: false
    })
  },
  /**
   * 修改个人号移动群聊/好友的分组列表
   * @param {[type]} data [description]
   */
  // getAccountGroupList(data) {
  //   return axios({
  //     url: `${API_MODULE}/change/group_groups`,
  //     method: 'PUT',
  //     responseType: 'json',
  //     data
  //   })
  // },
  /**
   * 移动好友到其它分组
   * @param {[type]} data [description]
   */
  moveFriendToGroup(data) {
    return axios({
      url: `${API_MODULE}/change/user_groups`,
      method: 'PUT',
      responseType: 'json',
      data
    })
  },
  /**
   * 移动群组/好友到其它分组
   * @param {[type]} data [description]
   */
  moveGroupChatToGroup(data) {
    return axios({
      url: `${API_MODULE}/change/group_groups`,
      method: 'PUT',
      responseType: 'json',
      data
    })
  },
  /**
   * 新建红包
   * @param {[type]} data [description]
   */
  newRedPacket(data) {
    return axios({
      url: `/manager/v1.0/redpage_send`,
      method: 'POST',
      data,
      removeEmpty
    })
  },
  /**
   * 朋友圈列表
   *
   * @param {*} params
   * @returns
   */
  listCircle(params) {
    return axios({
      url: `${API_MODULE}/circle`,
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 是否可以自动回复
   */
  getAutoReplyStatus() {
    return axios({
      url: `${API_MODULE}/auto_reply`,
      method: 'GET',
      removeEmpty
    })
  },
  /**
   * 关闭/开启自动回复
   */
  setAutoReplyStatus(status, loading = false) {
    return axios({
      url: `${API_MODULE}/auto_reply/${status}`,
      method: 'PUT',
      loading
    })
  },
  /**
   * 获取用户发来的消息对应的自动回复内容
   */
  getAutoReplyWithCustomerMsg(data) {
    return axios({
      url: `${API_MODULE}/auto_reply/reply_content`,
      method: 'POST',
      data,
      loading: false,
      removeEmpty
    })
  },
  /**
   * 统计收发消息进入redis信息
   */
  countMessage(data) {
    return axios({
      url: `${API_MODULE}/msg/count`,
      method: 'POST',
      data,
      loading: false,
      errorHandle: false,
      removeEmpty
    })
  },
  /**
   * 微客服，我的收藏列表
   */
  getAccountCollection() {
    return axios({
      url: `${API_MODULE}/collection`,
      method: 'GET',
      removeEmpty
    })
  },
  /**
   * 微客服，删除我的收藏列表的收藏
   */
  getDelCollectionId(id) {
    return axios({
      url: `${API_MODULE}/collection/${id}`,
      method: 'DELETE',
      responseType: 'json'
    })
  },
  /**
   * 转接客户，获取部门列表
   */
  getDepartmentList() {
    return axios({
      url: '/manager/v1.0/department',
      method: 'GET'
    })
  },
  /**
   * 部门下的员工列表
   */
  getDepartmentStaffList(params) {
    return axios({
      url: '/manager/v1.0/staff',
      method: 'GET',
      params
    })
  },
  /**
   * 把客户转接出去
   */
  transferCustomer(data) {
    return axios({
      url: `${API_MODULE}/transfer/chat`,
      method: 'PUT',
      data,
      removeEmpty
    })
  },
  /**
   * 获取消息推送用的的好友列表，不分组
   */
  searchCustomerServerFrenidList(data) {
    return axios({
      url: `${API_MODULE}/wechat_user/push`,
      method: 'POST',
      data,
      removeEmpty
    })
  },
  /**
   * 获取消息推送用的的群列表，不分组
   */
  searchCustomerServerGroupList(data) {
    return axios({
      url: `${API_MODULE}/wechat_group/push`,
      method: 'POST',
      data,
      removeEmpty
    })
  },
  /**
   * 获取需要拦截的微信敏感词
   */
  searchSensitiveWords() {
    return axios({
      url: `${API_MODULE}/risk`,
      method: 'GET',
      removeEmpty
    })
  },
  /**
   * 保存敏感记录
   */
  saveSensitiveRecord(data) {
    return axios({
      url: `${API_MODULE}/risk`,
      method: 'POST',
      data,
      removeEmpty
    })
  },
  /**
   * 保存调研词
   */
  saveCustomerRecord(data) {
    return axios({
      url: `${API_MODULE}/risk/customer`,
      method: 'POST',
      data,
      removeEmpty
    })
  },
  // ToDo
  // 保存群发消息记录
  setPushMessage(data) {
    return axios({
      url: `${API_MODULE}/push`,
      method: 'POST',
      data,
      removeEmpty
    })
  },
  // 修改群发消息状态
  putPushMessageStauts(data) {
    return axios({
      url: `${API_MODULE}/push`,
      method: 'PUT',
      data,
      loading: false,
      removeEmpty
    })
  },
  // 获取群发记录列表
  getPushMessageRecord(params) {
    return axios({
      url: `${API_MODULE}`,
      method: 'GET',
      params,
      removeEmpty
    })
  },
  // 获取群发记录详情
  getPushMessageDetailLogs(params) {
    return axios({
      url: `${API_MODULE}/logs`,
      method: 'GET',
      params,
      removeEmpty
    })
  },

  // 设置待办
  setTodo(data) {
    return axios({
      url: '/manager/v1.0/schedule',
      method: 'POST',
      data,
      removeEmpty
    })
  },
  // 获取待办项（已完成 + 未完成）
  getSetTodoList() {
    return axios({
      url: '/manager/v1.0/schedule',
      method: 'GET',
      removeEmpty
    })
  },
  finishStatus(params) {
    return axios({
      url: '/manager/v1.0/schedule',
      method: 'PUT',
      params,
      removeEmpty
    })
  },
  updateToDo(data) {
    return axios({
      url: '/manager/v1.0/schedule/update',
      method: 'PUT',
      data,
      removeEmpty
    })
  },
  deleteToDo(params) {
    return axios({
      url: '/manager/v1.0/schedule',
      method: 'DELETE',
      params,
      removeEmpty
    })
  },
  /**
   * 关闭/开启自动回复
   */
  setAiAutoReplyStatus(status, loading = false) {
    return axios({
      url: `${API_MODULE}/aiauto_reply/${status}`,
      method: 'PUT',
      loading
    })
  },
  /**
   * Ai是否可以自动回复
   */
  getAiautoEeply() {
    return axios({
      url: `${API_MODULE}/aiauto_reply`,
      method: 'GET',
      removeEmpty
    })
  }

}
