import axios from 'axios'
import {
  aiApi
} from '@/config'

export default {
  // 新建快捷语接口
  insertQuickMsg(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/insertUserTp`,
      method: 'post',
      data,
      responseType: 'json',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      dataToUnderline: false
    })
  },

  // 修改快捷语
  updateQuickMsg(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/updateUserTp`,
      method: 'post',
      responseType: 'json',
      data,
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      dataToUnderline: false
    })
  },

  // 删除快捷语
  deleteQuickMsg(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/removeUserTp`,
      method: 'post',
      responseType: 'json',
      data,
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      dataToUnderline: false
    })
  },

  // 查询快捷语接口
  searchQuickMsg(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/queryPage`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false
    })
  },
  // OCR识别接口
  ocrProcessImg(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/solution/ocrProcess`,
      method: 'post',
      responseType: 'json',
      dataToUnderline: false,
      data
    })
  },
  // OCR识别接口
  ocrUpdate(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/solution/ocrUpdate`,
      method: 'post',
      responseType: 'json',
      dataToUnderline: false,
      data
    })
  },
  // 查询历史推荐话术
  historySkillQuery(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/solution/historySkillQuery`,
      method: 'POST',
      data,
      responseType: 'json',
      dataToUnderline: false,
      loading: false
    })
  },
  // 设置讯飞AI自动回复
  setAutoAiReplyStatus(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/systemConfig/setAIAuto`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false
    })
  },
  // 讯飞输入框查询
  queryTopQuestions(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/queryTopQuestions`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false,
      loading: false
    })
  },
  // 讯飞ai收藏保存
  processAISpeech(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/processAISpeech`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false
    })
  },
  // 讯飞删除
  removeAISpeech(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/removeAISpeech`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false
    })
  },
  // 讯飞查询
  querySpeechs(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/querySpeechs`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false
    })
  },
  // 小程序点击量
  kickWechat(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/systemConfig/kickWechat`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false,
      loading: false
    })
  },
  // 查询表情包
  queryEmotionInfos(data) {
    return axios({
      url: `${aiApi}/microservice/api/v1/template/queryEmotionInfos`,
      method: 'post',
      responseType: 'json',
      data,
      applicationForm: true,
      dataToUnderline: false,
      loading: false
    })
  }

}
