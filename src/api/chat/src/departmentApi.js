import axios from 'axios'
const API_MODULE = `/manager/v1.0/department`

export default {
  /**
   * 转接客户，获取部门列表
   */
  getDepartmentList() {
    return axios({
      url: `${API_MODULE}/department`,
      method: 'GET'
    })
  }
}
