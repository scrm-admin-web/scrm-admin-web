import axios from 'axios'
const API_MODULE_V = `/oem/v1.0/`

export default {
  /**
   * 企业列表
   *
   * @param {*} params
   * @returns
   */
  enterList(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 企业详情
   *
   * @param {*} params
   * @returns
   */
  enterDetails(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/${params}`,
      method: 'GET'
    })
  },
  /**
   * 设备列表
   *
   * @param {*} params
   * @returns
   */
  enterListEuq(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/euq`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 店铺列表
   *
   * @param {*} params
   * @returns
   */
  enterListShop(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/shop`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 个人号列表
   *
   * @param {*} params
   * @returns
   */
  enterListAccount(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/account`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 增加时长
   *
   * @param {*} params
   * @returns
   */
  enterAddTime(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/time`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 新增设备
   *
   * @param {*} params
   * @returns
   */
  enterAddEqu(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/equ`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 新增店铺
   *
   * @param {*} params
   * @returns
   */
  enterAddShop(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/shop`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 新增个人号
   *
   * @param {*} params
   * @returns
   */
  enterAddAccount(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/account`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 禁用/启用
   *
   * @param {*} params
   * @returns
   */
  enterIsable(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/isable`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 重置密码
   *
   * @param {*} params
   * @returns
   */
  enterReset(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/reset`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 新增企业
   *
   * @param {*} params
   * @returns
   */
  enterAdd(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 企业详情-编辑用
   *
   * @param {*} params
   * @returns
   */
  enterEditDetails(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/edit/${params}`,
      method: 'GET'
    })
  },
  /**
   * 编辑企业
   *
   * @param {*} params
   * @returns
   */
  enterEdit(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 可用设备数
   *
   * @param {*} params
   * @returns
   */
  equNum(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/equ/num`,
      method: 'GET'
    })
  },
  /**
   * 可用店铺数
   *
   * @param {*} params
   * @returns
   */
  shopNum(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/shop/num`,
      method: 'GET'
    })
  },
  /**
   * 可用个人号数
   *
   * @param {*} params
   * @returns
   */
  accountNum(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/account/num`,
      method: 'GET'
    })
  },
  /**
   * 可用企业数
   *
   * @param {*} params
   * @returns
   */
  shopEnter(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/shop/enter`,
      method: 'GET'
    })
  },
  /**
   * 可用工作群
   *
   * @param {*} params
   * @returns
   */
  groupNum(params) {
    return axios({
      url: `${API_MODULE_V}customer/proxy/group/num`,
      method: 'GET'
    })
  },
  /**
   * 新增群
   *
   * @param {*} params
   * @returns
   */
  groupAdd(params) {
    return axios({
      url: `${API_MODULE_V}customer/enter/group`,
      method: 'POST',
      data: params
    })
  }
}
