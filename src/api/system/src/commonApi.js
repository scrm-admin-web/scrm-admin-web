import axios from 'axios'
const API_MODULE_V = `/common/v1.0/`
const removeEmpty = true

export default {
  /**
   * 部门列表获取
   *
   * @param {*} params
   * @returns
   */
  departmentList(params) {
    return axios({
      url: `${API_MODULE_V}search/department`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 员工列表获取
   *
   * @param {*} params
   * @returns
   */
  staffList(params) {
    return axios({
      url: `${API_MODULE_V}search/staff`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 角色列表获取
   *
   * @param {*} params
   * @returns
   */
  roleList() {
    return axios({
      url: `${API_MODULE_V}search/role/list`,
      method: 'GET'
    })
  },
  /**
   * 店铺列表
   *
   * @param {*} params
   * @returns
   */
  storeList(params) {
    return axios({
      url: `${API_MODULE_V}shop_order/list/shop`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 查询订单详情
   *
   * @param {*} params
   * @returns
   */
  getOrder(orderNo) {
    return axios({
      url: `${API_MODULE_V}shop_order/order/${orderNo}`,
      method: 'GET'
    })
  },
  /**
   * 获取微活码信息列表
   *
   * @param {*} params
   * @returns
   */
  microCodeList() {
    return axios({
      url: `${API_MODULE_V}search/micro_code/list`,
      method: 'GET'
    })
  },
  /**
   * 获取系统配置
   * enterpriseId 贴牌商id 公共传0
   * key version app版本号 domain短域名redpage红包费率% message短信多少钱一条 taobao_template淘宝订单模板 message_template 短信模板
   * @param {*} params
   * @returns
   */
  systemConfig(key, enterpriseId) {
    let enterpriseIdTemp = enterpriseId || 0
    return axios({
      url: `${API_MODULE_V}config/${enterpriseIdTemp}/${key}`,
      method: 'GET'
    })
  },
  /* 获取部门树列表
   *
   * @param {*} params
   * @returns
   */
  departmentTreeList(params) {
    return axios({
      url: `${API_MODULE_V}search`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 获取--微信好友列表分组
   *
   * @param {*} params
   * @returns
   */
  personNumFriendGroup(params) {
    return axios({
      url: `${API_MODULE_V}search/wx_group`,
      method: 'GET'
    })
  },
  /**
   * 获取--微信群分组
   *
   * @param {*} params
   * @returns
   */
  personNumGroupGroup(params) {
    return axios({
      url: `${API_MODULE_V}search/group_group`,
      method: 'GET'
    })
  },
  /**
   * 获取系统配置
   *
   * @param {*} params
   * @returns
   */
  configEnterprise(params) {
    return axios({
      url: `${API_MODULE_V}config/${params.enterpriseId}/${params.key}`,
      method: 'GET'
    })
  },
  /**
   * 查询菜单权限
   * @param {*} params
   */
  getMenuRight(params) {
    return axios({
      url: `${API_MODULE_V}role/menu/role`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 查询功能权限列表
   * @param {*} params
   */
  getFunctionRight(params) {
    return axios({
      url: `${API_MODULE_V}role/action/role/`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 查询功能权限列表
   * @param {*} params
   */
  getOrderDetails(params) {
    return axios({
      url: `${API_MODULE_V}shop_order/order/${params}`,
      method: 'GET'
    })
  },
  /**
   * 获取支付方式
   *
   * @param {*} params
   * @returns
   */
  getPayTypeReq(params) {
    return axios({
      url: `${API_MODULE_V}pay/pay_type`,
      method: 'GET'
    })
  },
  /**
   * 获取当前企业的剩余短信数量
   *
   * @param {*} params
   * @returns
   */
  getgMessageLeft(params) {
    return axios({
      url: `${API_MODULE_V}msg/message_left`,
      method: 'GET'
    })
  },
  /**
   * 获取分组列表接口
   *
   * @param {*} params
   * @returns
   */
  getGroupList(params) {
    return axios({
      url: `${API_MODULE_V}search/group_info`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 获取微客服拥有的个人号列表（新）
   */
  getPersonalAccountListNewSec() {
    return axios({
      url: `${API_MODULE_V}search/wechat_account_group`,
      method: 'GET'
    })
  },
  /**
   * 微信群列表接口
   */
  getWxList() {
    return axios({
      url: `${API_MODULE_V}search/group/list`,
      method: 'GET'
    })
  }

}
