import axios from 'axios'
const API_MODULE = `/manager/v1.0/risk/`

export default {
    // 微信风控
    /**
     * 敏感词设置-敏感词列表
     *
     * @param {*} data
     * @returns
     */
    wordConfigList(data) {
        return axios({
            url: `${API_MODULE}wechat/word_config`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 敏感词设置-批量增加敏感词
     *
     * @param {*} data
     * @returns
     */
    addWordConfig(data) {
        return axios({
            url: `${API_MODULE}wechat/word_config`,
            method: 'POST',
            data: data
        })
    },
    /**
     * 敏感词设置-编辑微信风控敏感词
     *
     * @param {*} data
     * @returns
     */
    editWordConfig(data) {
        return axios({
            url: `${API_MODULE}wechat/word_config`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 敏感词设置-编辑微信风控敏感词
     *
     * @param {*} data
     * @returns
     */
    deleteWordConfig(data) {
        return axios({
            url: `${API_MODULE}wechat/word_config/${data}`,
            method: 'DELETE'
        })
    },
    /**
     * 敏感词设置-获取分组
     *
     * @param {*} data
     * @returns
     */
    wordList(data) {
        return axios({
            url: `${API_MODULE}wechat/word_config`,
            method: 'GET'
        })
    },
    /**
     * 敏感操作-获取敏感操作日志表列表
     *
     * @param {*} data
     * @returns
     */
    operateList(data) {
        return axios({
            url: `${API_MODULE}wechat/operate`,
            method: 'GET',
            removeEmpty: true,
            params: data,
            timeout: 180000
        })
    },
    /**
     * 敏感操作-敏感操作类型列表
     *
     * @param {*} data
     * @returns
     */
    sensitiveTypeList(data) {
        return axios({
            url: `${API_MODULE}wechat/operate/sensitive_type`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 敏感操作-导出联系人信息
     *
     * @param {*} data
     * @returns
     */
    operateExport(data) {
        return axios({
            url: `${API_MODULE}wechat/operate/export`,
            method: 'GET',
            removeEmpty: true,
            params: data,
           // responseType: 'blob',
            timeout: 180000
        })
    },
    /**
     * 敏感操作-编辑敏感操作类型
     *
     * @param {*} data
     * @returns
     */
    editSensitiveType(data) {
        return axios({
            url: `${API_MODULE}wechat/operate/sensitive_type`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 敏感操作-处理
     *
     * @param {*} data
     * @returns
     */
    operateDeal(data) {
        return axios({
            url: `${API_MODULE}wechat/operate/deal`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 敏感操作-详情
     *
     * @param {*} data
     * @returns
     */
    getWordConfig(data) {
        return axios({
            url: `${API_MODULE}wechat/word_config/${data}`,
            method: 'GET'
        })
    },
    /**
     * 敏感通知-获取敏感通知配置列表
     *
     * @param {*} data
     * @returns
     */
    wechatNoticeConfigList(data) {
        return axios({
            url: `${API_MODULE}wechat/notice_config`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 敏感通知-获取发送人
     *
     * @param {*} data
     * @returns
     */
    wechatGetNoticeConfigSent(data) {
        return axios({
            url: `${API_MODULE}wechat/notice_config/sent`,
            method: 'GET'
        })
    },
    /**
     * 敏感通知-设置发送人
     *
     * @param {*} data
     * @returns
     */
    wechatNoticeConfigSent(data) {
        return axios({
            url: `${API_MODULE}wechat/notice_config/sent`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 敏感通知-修改敏感通知配置
     *
     * @param {*} data
     * @returns
     */
    wechatEditNoticeConfig(data) {
        return axios({
            url: `${API_MODULE}wechat/notice_config`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 重复好友统计-获取重复好友列表
     *
     * @param {*} data
     * @returns
     */
    getRrepeatFrends(data) {
        return axios({
            url: `${API_MODULE}wechat/repeat_frends`,
            method: 'GET',
            params: data
        })
    },
    /**
     * 重复好友统计-获取重复好友详情列表
     *
     * @param {*} data
     * @returns
     */
    repeatFrendsList(data) {
        return axios({
            url: `${API_MODULE}wechat/repeat_frends/detail`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 重复好友统计-导出
     *
     * @param {*} data
     * @returns
     */
    repeatFrendsExport(data) {
        return axios({
            url: `${API_MODULE}wechat/repeat_frends/export`,
            method: 'GET',
            responseType: 'blob'
        })
    },
    /**
     * 重复群统计-获取重复群列表
     *
     * @param {*} data
     * @returns
     */
    repeatGroups(data) {
        return axios({
            url: `${API_MODULE}wechat/repeat_groups`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 重复群统计-获取重复群详情列表
     *
     * @param {*} data
     * @returns
     */
    repeatGroupsList(data) {
        return axios({
            url: `${API_MODULE}wechat/repeat_groups/detail`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 重复群统计-导出
     *
     * @param {*} data
     * @returns
     */
    repeatGroupsExport(data) {
        return axios({
            url: `${API_MODULE}wechat/repeat_groups/export`,
            method: 'GET',
            responseType: 'blob'
        })
    },
    /**
     * 微信好友导出-微信好友列表
     *
     * @param {*} data
     * @returns
     */
    friendList(data) {
        return axios({
            url: `${API_MODULE}wechat/friend`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 微信好友导出-批量导出联系人
     *
     * @param {*} data
     * @returns
     */
    friendExprot(data) {
        return axios({
            url: `${API_MODULE}wechat/friend/exprot/contact`,
            method: 'POST',
            data: data,
            responseType: 'blob'
        })
    },
    /**
     * 微信好友导出-导出全部好友
     *
     * @param {*} data
     * @returns
     */
    // friendExprotAll(data) {
    //   return axios({
    //     url: `${API_MODULE}wechat/friend/exprot/all/contact`,
    //     method: 'POST',
    //     data: data,
    //     responseType: 'string',
    //     timeout:180000
    //   })
    // },
    friendExprotAll(data) {
        return axios({
            url: `${API_MODULE}wechat/friend/exprot/all/contact/time`,
            method: 'POST',
            data: data,
            responseType: 'string',
            timeout: 180000
        })
    },
    /**
     * 微信好友导出-导出聊天记录
     *
     * @param {*} data
     * @returns
     */
    friendExprotChatHistory(data) {
        return axios({
            url: `${API_MODULE}wechat/friend/exprot/chat_history`,
            method: 'POST',
            data: data,
            responseType: 'blob'
        })
    },
    /**
     * 微信财务统计-微信个人号列表
     *
     * @param {*} data
     * @returns
     */
    financeList(data) {
        return axios({
            url: `${API_MODULE}wechat/finance`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 微信财务统计-导出
     *
     * @param {*} data
     * @returns
     */
    financeExport(data) {
        return axios({
            url: `${API_MODULE}wechat/finance/export`,
            method: 'GET',
            params: data,
            responseType: 'blob'
        })
    },
    // 手机风控
    /**
     * 通话录音-获取通话录音列表
     *
     * @param {*} data
     * @returns
     */
    recordList(data) {
        return axios({
            url: `${API_MODULE}mobile/record`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 通话录音-获取详情
     *
     * @param {*} data
     * @returns
     */
    recordListDetail(data) {
        return axios({
            url: `${API_MODULE}mobile/record/${data}`,
            method: 'GET',
            removeEmpty: true
        })
    },
    // 通话录音导出
    recordExpoet(params) {
        return axios({
            url: `${API_MODULE}mobile/record/exprot`,
            method: 'GET',
            params: params,
            responseType: 'blob'
        })
    },
    /**
     * 通话录音-重新转码
     *
     * @param {*} data
     * @returns
     */
    recordListChangeAgain(data) {
        return axios({
            url: `${API_MODULE}mobile/record/${data}`,
            method: 'POST'
        })
    },
    /**
     * 通话录音-删除录音
     *
     * @param {*} data
     * @returns
     */
    deleteRecord(data) {
        return axios({
            url: `${API_MODULE}mobile/record/${data}`,
            method: 'DELETE'
        })
    },
    /**
     * 短信敏感词-敏感短信配置列表
     *
     * @param {*} data
     * @returns
     */
    sensitiveConfigList(data) {
        return axios({
            url: `${API_MODULE}mobile/sensitive_config`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 短信敏感词-增加敏感词
     *
     * @param {*} data
     * @returns
     */
    addSensitiveConfig(data) {
        return axios({
            url: `${API_MODULE}mobile/sensitive_config`,
            method: 'POST',
            data: data
        })
    },
    /**
     * 短信敏感词-更新敏感词
     *
     * @param {*} data
     * @returns
     */
    editSensitiveConfig(data) {
        return axios({
            url: `${API_MODULE}mobile/sensitive_config`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 短信敏感词-删除
     *
     * @param {*} data
     * @returns
     */
    deleteSensitiveConfig(data) {
        return axios({
            url: `${API_MODULE}mobile/sensitive_config/${data}`,
            method: 'DELETE'
        })
    },
    /**
     * 手机短信-手机短信列表
     *
     * @param {*} data
     * @returns
     */
    messageList(data) {
        return axios({
            url: `${API_MODULE}mobile/message`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 手机短信-导出
     *
     * @param {*} data
     * @returns
     */
    messageExpoet(params) {
        return axios({
            url: `${API_MODULE}mobile/message/expoet`,
            method: 'GET',
            params: params,
            responseType: 'blob'
        })
    },
    /**
     * 手机短信-删除
     *
     * @param {*} data
     * @returns
     */
    deleteMessage(data) {
        return axios({
            url: `${API_MODULE}mobile/message/${data}`,
            method: 'DELETE'
        })
    },
    /**
     * 敏感短信-敏感短信列表
     *
     * @param {*} data
     * @returns
     */
    sensitiveList(data) {
        return axios({
            url: `${API_MODULE}mobile/sensitive`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 敏感短信-批量处理
     *
     * @param {*} data
     * @returns
     */
    sensitiveListDeal(data) {
        return axios({
            url: `${API_MODULE}mobile/sensitive/deal`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 敏感短信-导出
     *
     * @param {*} data
     * @returns
     */
    sensitiveListExpoet() {
        return axios({
            url: `${API_MODULE}mobile/sensitive/expoet`,
            method: 'GET',
            responseType: 'blob'
        })
    },
    /**
     * 敏感短信-详情
     *
     * @param {*} data
     * @returns
     */
    sensitiveListDetails(data) {
        return axios({
            url: `${API_MODULE}mobile/sensitive/${data}`,
            method: 'GET'
        })
    },
    /**
     * 敏感通知-获取敏感通知配置列表
     *
     * @param {*} data
     * @returns
     */
    noticeConfigList(data) {
        return axios({
            url: `${API_MODULE}mobile/notice_config`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 敏感通知-获取发送人
     *
     * @param {*} data
     * @returns
     */
    getNoticeConfigSent(data) {
        return axios({
            url: `${API_MODULE}mobile/notice_config/sent`,
            method: 'GET'
        })
    },
    /**
     * 敏感通知-设置发送人
     *
     * @param {*} data
     * @returns
     */
    noticeConfigSent(data) {
        return axios({
            url: `${API_MODULE}mobile/notice_config/sent`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 敏感通知-修改敏感通知配置
     *
     * @param {*} data
     * @returns
     */
    editNoticeConfig(data) {
        return axios({
            url: `${API_MODULE}mobile/notice_config`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * APP设置-应用列表
     *
     * @param {*} data
     * @returns
     */
    appList(data) {
        return axios({
            url: `${API_MODULE}mobile/app`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * APP设置-新增app
     *
     * @param {*} data
     * @returns
     */
    addApp(data) {
        return axios({
            url: `${API_MODULE}mobile/app`,
            method: 'POST',
            data: data
        })
    },
    /**
     * APP设置-编辑app
     *
     * @param {*} data
     * @returns
     */
    editApp(data) {
        return axios({
            url: `${API_MODULE}mobile/app`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * APP设置-设备查询
     *
     * @param {*} data
     * @returns
     */
    queryApp(data) {
        return axios({
            url: `${API_MODULE}mobile/app/eq`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * APP设置-分配权限
     *
     * @param {*} data
     * @returns
     */
    appAssign(data) {
        return axios({
            url: `${API_MODULE}mobile/app/assign`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 权限管理-权限列表
     *
     * @param {*} data
     * @returns
     */
    permissionList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理-设备查询
     *
     * @param {*} data
     * @returns
     */
    queryPermissionList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/eq`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理---获取设备列表
     *
     * @param {*} data
     * @returns
     */
    getEquipList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/equ`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理-分配权限
     *
     * @param {*} data
     * @returns
     */
    permissionListAssign(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/assign`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 权限管理---获取设备列表--移出
     *
     * @param {*} data
     * @returns
     */
    removeEquipList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/equ`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 权限管理---获取设备列表
     *
     * @param {*} data
     * @returns
     */
    getAddEquipList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/euq/choose`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理---应用管理列表
     *
     * @param {*} data
     * @returns
     */
    getAppManageList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/app`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理---应用设备列表
     *
     * @param {*} data
     * @returns
     */
    getAppEquipList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/app/list`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理---新增app设置
     *
     * @param {*} data
     * @returns
     */
    addAppSetting(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/app`,
            method: 'POST',
            removeEmpty: true,
            data
        })
    },
    /**
     * 权限管理---获取应用设备列表--移出
     *
     * @param {*} data
     * @returns
     */
    removeAppEquipList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/app`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 权限管理---获取使用管理--类型列表
     *
     * @param {*} data
     * @returns
     */
    getUseManageTypeList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/use`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理---获取使用管理--数据列表
     *
     * @param {*} data
     * @returns
     */
    getUseManageDataList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/use/list`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理---获取使用管理--使用管理-批量解机/锁机/回复出产设置
     *
     * @param {*} data
     * @returns
     */
    getUseManageDataModify(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/use`,
            method: 'PUT',
            data: data
        })
    },
    /**
     * 权限管理---获取使用管理--使用管理-批量解机/锁机/回复出产设置
     *
     * @param {*} data
     * @returns
     */
    getTimeAndLocationList(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/use/equ_use`,
            method: 'GET',
            removeEmpty: true,
            params: data
        })
    },
    /**
     * 权限管理---获取使用管理--使用管理-时间/地理围栏 配置详情
     *
     * @param {*} data
     * @returns
     */
    getTimeAndLocationDetail(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/use/equ_use/${data}`,
            method: 'GET'
        })
    },
    /**
     * 权限管理---获取使用管理--使用管理-时间/地理围栏 配置详情--设置
     *
     * @param {*} data
     * @returns
     */
    setTimeAndLocationDetail(data) {
        return axios({
            url: `${API_MODULE}mobile/permission/use/equ_use`,
            method: 'PUT',
            data: data
        })
    },

    // 录音转文本
    callrecordToText(data) {
        return axios({
            url: `${API_MODULE}mobile/record/asr`,
            method: 'PUT',
            params: data
        })
    }
}
