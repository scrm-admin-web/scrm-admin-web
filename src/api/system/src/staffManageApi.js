import axios from 'axios'
const API_MODULE = `/manager/v1.0`

export default {
  // 员工管理
  /**
   * 获取角色列表
   *
   * @param {*} params
   * @returns
   */
  getRoleList(params) {
    return axios({
      url: `${API_MODULE}/role`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取部门列表
   *
   * @param {*} params
   * @returns
   */
  getDepartmentList() {
    return axios({
      url: `${API_MODULE}/department`,
      removeEmpty: true,
      method: 'GET'
    })
  },
  /**
   * 编辑部门
   * @param {*} params
   */
  editorDepartment(params) {
    return axios({
      url: `${API_MODULE}/department`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取父级部门
   */
  getParentDepartment() {
    return axios({
      url: `${API_MODULE}/department/get/parment`,
      method: 'GET'
    })
  },
  /**
   * 新增部门
   */
  addDepartment(params) {
    return axios({
      url: `${API_MODULE}/department/add/department`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 删除部门
   */
  deleteDepartment(id) {
    return axios({
      url: `${API_MODULE}/department/${id}`,
      method: 'DELETE'
    })
  },
  /**
   * 获取部门员工
   */
  getDepartmenStaff(params) {
    return axios({
      url: `${API_MODULE}/staff`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取部门员工详情根据id
   */
  getDepartmenStaffById(id) {
    return axios({
      url: `${API_MODULE}/staff/${id}`,
      method: 'GET'
    })
  },
  /**
   * 数据矫正
   */
  dataCorrect(params) {
    return axios({
      url: `${API_MODULE}/staff/all_data_correct`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 新增员工
   */
  newStaff(params) {
    return axios({
      url: `${API_MODULE}/staff`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 编辑员工
   * @param {*} params
   */
  editorStaff(params) {
    return axios({
      url: `${API_MODULE}/staff/update/staff`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 重置员工密码
   * @param {*} params
   */
  resetStafffPassWord(params) {
    return axios({
      url: `${API_MODULE}/staff/update/password`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 启用禁用账号
   */
  changeStaffStatus(params) {
    return axios({
      url: `${API_MODULE}/staff/update/status`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 角色禁用/启用
   */
  changeRoleStatus(params) {
    return axios({
      url: `${API_MODULE}/role/update/role`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 查询功能权限
   */
  getFunctionRight(params) {
    return axios({
      url: `${API_MODULE}/role/action/role`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 查询菜单权限
   */
  getMenuRight(params) {
    return axios({
      url: `${API_MODULE}/role/menu/role`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 查询数据权限
   */
  getDataRight(params) {
    return axios({
      url: `${API_MODULE}/role/data/role`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 删除角色
   */
  deleteRole(id) {
    return axios({
      url: `${API_MODULE}/role/${id}`,
      method: 'DELETE'
    })
  },
  /**
   * 添加角色权限
   */
  addSureRole(params) {
    return axios({
      url: `${API_MODULE}/role`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 更新角色(权限)
   */
  editSureRole(params) {
    return axios({
      url: `${API_MODULE}/role/update/role/all`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取--导入员工--下载模板
   *
   * @param {*} params
   * @returns
   */
  getImportStaffTemplate() {
    return axios({
      url: `${API_MODULE}/staff/staff_template`,
      method: 'GET'
    })
  },
  /**
   * 获取--导入员工
   *
   * @param {*} params
   * @returns
   */
  importStaff(data) {
    return axios({
      url: `${API_MODULE}/staff/import/csv`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: data
    })
  },
  /**
   * 手机管理-测试按钮导出
   *
   *
   */
  exportList(params) {
    return axios({
      url: `${API_MODULE}/department/wxaccountstreamexport`,
      method: 'GET',
      data: params,
      responseType: 'blob'
    })
  },
  /**
   *
   * 导出按钮显示
   */
  showBtn(params) {
    return axios({
      url: `${API_MODULE}/department/canexport`,
      method: 'GET',
      data: params
    })
  },

  // 后端备用的导出文件
  exportList1(params) {
    return axios({
      url: `${API_MODULE}/department/wxaccountstreamexport2`,
      method: 'GET',
      data: params,
      responseType: 'blob'
    })
  }

}
