import axios from 'axios'
const API_MODULE = `/manager/v2.0`

export default {
  /**
   * 营销平台-首绑有礼-获取首绑有礼列表
   *
   * @param {*} params
   * @returns
   */
  activityBindList(params) {
    return axios({
      url: `${API_MODULE}/activityBind`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-首绑有礼-新增首绑有礼
   *
   * @param {*} params
   * @returns
   */
  activityBindAdd(params) {
    return axios({
      url: `${API_MODULE}/activityBind`,
      method: 'POST',
      data: params,
      removeEmpty: true
    })
  },
  /**
   * 营销平台-首绑有礼-编辑首绑有礼
   *
   * @param {*} params
   * @returns
   */
  activityBindEdit(params) {
    return axios({
      url: `${API_MODULE}/activityBind`,
      method: 'PUT',
      data: params,
      removeEmpty: true
    })
  },
  /**
   * 营销平台-首绑有礼-首绑活动统计
   *
   * @param {*} params
   * @returns
   */
  statisticsBind(params) {
    return axios({
      url: `${API_MODULE}/activityBind/statistics`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 营销平台-首绑有礼-活动上线、下线
   *
   * @param {*} params
   * @returns
   */
  updateStatusBind(params) {
    return axios({
      url: `${API_MODULE}/activityBind/updateStatus/${params}`,
      method: 'PUT'
    })
  },
  /**
   * 营销平台-首绑有礼-获取首绑有礼详情
   *
   * @param {*} params
   * @returns
   */
  detailsBind(params) {
    return axios({
      url: `${API_MODULE}/activityBind/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-首绑有礼-删除首绑有礼信息
   *
   * @param {*} params
   * @returns
   */
  deleteBind(params) {
    return axios({
      url: `${API_MODULE}/activityBind/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 营销平台-首绑有礼-获取首绑有礼—绑定记录表列表
   *
   * @param {*} params
   * @returns
   */
  activityBindLog(params) {
    return axios({
      url: `${API_MODULE}/activityBindLog`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-首绑有礼-预览首绑活动
   *
   * @param {*} params
   * @returns
   */
  activityBindPreview(params) {
    return axios({
      url: `${API_MODULE}/activityBind/preview`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-追评有礼-获取追评价有礼列表
   *
   * @param {*} params
   * @returns
   */
  commentList(params) {
    return axios({
      url: `${API_MODULE}/activity/comment`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-追评有礼-新增追评价有礼
   *
   * @param {*} params
   * @returns
   */
  commentAdd(params) {
    return axios({
      url: `${API_MODULE}/activity/comment`,
      method: 'POST',
      data: params,
      removeEmpty: true
    })
  },
  /**
   * 营销平台-追评有礼-编辑追评活动
   *
   * @param {*} params
   * @returns
   */
  commentEdit(params) {
    return axios({
      url: `${API_MODULE}/activity/comment`,
      method: 'PUT',
      data: params,
      removeEmpty: true
    })
  },
  /**
   * 营销平台-追评有礼-获取追评价有礼详情-统计
   *
   * @param {*} params
   * @returns
   */
  commentDetail(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/detail`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-追评有礼-下线追评活动
   *
   * @param {*} params
   * @returns
   */
  commentOffline(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/offline/${params}`,
      method: 'PUT'
    })
  },
  /**
   * 营销平台-追评有礼-追评有礼信息预览
   *
   * @param {*} params
   * @returns
   */
  commentPreview(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/preview`,
      method: 'POST',
      data: params,
      removeEmpty: true
    })
  },
  /**
   * 营销平台-追评有礼-获取追评价有礼详情
   *
   * @param {*} params
   * @returns
   */
  commentDetailId(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-追评有礼-删除追评活动
   *
   * @param {*} params
   * @returns
   */
  commentDelete(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 营销平台-追评有礼-获取支出记录
   *
   * @param {*} params
   * @returns
   */
  commentPay(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/pay`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-追评有礼-获取支出统计
   *
   * @param {*} params
   * @returns
   */
  commentPayTotal(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/pay/total`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-追评有礼-获取申请列表
   *
   * @param {*} params
   * @returns
   */
  commentCheck(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/check`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-追评有礼-通过申请
   *
   * @param {*} params
   * @returns
   */
  commentCheckPass(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/check/pass`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-追评有礼-拒绝申请
   *
   * @param {*} params
   * @returns
   */
  commentCheckRefer(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/check/refer`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-追评有礼-获取申请详情
   *
   * @param {*} params
   * @returns
   */
  commentCheckId(params) {
    return axios({
      url: `${API_MODULE}/activity/comment/check/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-加购有礼-获取加购有礼列表
   *
   * @param {*} params
   * @returns
   */
  buyagainList(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-加购有礼-新增加购有礼
   *
   * @param {*} params
   * @returns
   */
  buyagainAdd(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain`,
      method: 'POST',
      data: params,
      removeEmpty: true
    })
  },
  /**
   * 营销平台-加购有礼-编辑加购有礼
   *
   * @param {*} params
   * @returns
   */
  buyagainEdit(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-加购有礼-获取加购有礼详情-统计
   *
   * @param {*} params
   * @returns
   */
  buyagainDetails(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/detail`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-加购有礼-下线加购活动
   *
   * @param {*} params
   * @returns
   */
  buyagainOffline(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/offline/${params}`,
      method: 'PUT',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-加购有礼-加购有礼信息预览
   *
   * @param {*} params
   * @returns
   */
  buyagainPreview(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/preview`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-加购有礼-获取追评价有礼详情
   *
   * @param {*} params
   * @returns
   */
  buyagainDetailsId(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-加购有礼-删除加购有礼信息
   *
   * @param {*} params
   * @returns
   */
  buyagainDelete(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 营销平台-加购有礼-获取支出记录
   *
   * @param {*} params
   * @returns
   */
  buyagainPay(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/pay`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-加购有礼-获取支出统计
   *
   * @param {*} params
   * @returns
   */
  buyagainPayTotal(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/pay/total`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-加购有礼-获取申请列表
   *
   * @param {*} params
   * @returns
   */
  buyagainCheck(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/check`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-加购有礼-通过申请
   *
   * @param {*} params
   * @returns
   */
  buyagainCheckPass(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/check/pass`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-加购有礼-拒绝申请
   *
   * @param {*} params
   * @returns
   */
  buyagainCheckRefer(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/check/refer`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-加购有礼-获取申请详情
   *
   * @param {*} params
   * @returns
   */
  buyagainCheckId(params) {
    return axios({
      url: `${API_MODULE}/activity/buyagain/check/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-收藏有礼-获取收藏有礼分页信息
   *
   * @param {*} params
   * @returns
   */
  activityCollectionList(params) {
    return axios({
      url: `${API_MODULE}/activityCollection`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-收藏有礼-新增收藏有礼
   *
   * @param {*} params
   * @returns
   */
  activityCollectionAdd(params) {
    return axios({
      url: `${API_MODULE}/activityCollection`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-收藏有礼-编辑收藏有礼
   *
   * @param {*} params
   * @returns
   */
  activityCollectionEdit(params) {
    return axios({
      url: `${API_MODULE}/activityCollection`,
      method: 'PUT',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-收藏有礼-获取收藏有礼统计信息
   *
   * @param {*} params
   * @returns
   */
  activityCollectionCount(params) {
    return axios({
      url: `${API_MODULE}/activityCollection/count`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-收藏有礼-编辑收藏有礼下线
   *
   * @param {*} params
   * @returns
   */
  activityCollectionOffLine(params) {
    return axios({
      url: `${API_MODULE}/activityCollection/off_line/${params}`,
      method: 'PUT',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-收藏有礼-获取收藏有礼-支付记录支出合计信息
   *
   * @param {*} params
   * @returns
   */
  activityCollectionPlayCount(params) {
    return axios({
      url: `${API_MODULE}/activityCollection/play_count`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-收藏有礼-获取收藏有礼-支付记录分页信息
   *
   * @param {*} params
   * @returns
   */
  activityCollectionPlayInfo(params) {
    return axios({
      url: `${API_MODULE}/activityCollection/play_info`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-收藏有礼-收藏有礼信息预览
   *
   * @param {*} params
   * @returns
   */
  activityCollectionPreview(params) {
    return axios({
      url: `${API_MODULE}/activityCollection/preview`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-收藏有礼-获取收藏有礼详情
   *
   * @param {*} params
   * @returns
   */
  activityCollectionId(params) {
    return axios({
      url: `${API_MODULE}/activityCollection/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-收藏有礼-删除收藏有礼信息
   *
   * @param {*} params
   * @returns
   */
  activityCollectionDelete(params) {
    return axios({
      url: `${API_MODULE}/activityCollection/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 营销平台-收藏有礼-活动审核分页信息
   *
   * @param {*} params
   * @returns
   */
  activityCollectionLog(params) {
    return axios({
      url: `${API_MODULE}/activityCollectionLog`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-收藏有礼-收藏有礼活动审核
   *
   * @param {*} params
   * @returns
   */
  activityCollectionLogApply(params) {
    return axios({
      url: `${API_MODULE}/activityCollectionLog`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-新品投票-获取新品投票分页信息
   *
   * @param {*} params
   * @returns
   */
  activityVoteList(params) {
    return axios({
      url: `${API_MODULE}/activityVote`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-新品投票-新增新品投票
   *
   * @param {*} params
   * @returns
   */
  activityVoteAdd(params) {
    return axios({
      url: `${API_MODULE}/activityVote`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-新品投票-编辑新品投票
   *
   * @param {*} params
   * @returns
   */
  activityVoteEdit(params) {
    return axios({
      url: `${API_MODULE}/activityVote`,
      method: 'PUT',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-新品投票-获取新品投票统计信息
   *
   * @param {*} params
   * @returns
   */
  activityVoteCount(params) {
    return axios({
      url: `${API_MODULE}/activityVote/count`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-新品投票-活动下线编辑
   *
   * @param {*} params
   * @returns
   */
  activityVoteOffLine(params) {
    return axios({
      url: `${API_MODULE}/activityVote/off_line/${params}`,
      method: 'PUT'
    })
  },
  /**
   * 营销平台-新品投票-支付记录支出合计信息
   *
   * @param {*} params
   * @returns
   */
  activityVotePlayCount(params) {
    return axios({
      url: `${API_MODULE}/activityVote/play_count`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-新品投票-支付记录分页信息
   *
   * @param {*} params
   * @returns
   */
  activityVotePlayInfo(params) {
    return axios({
      url: `${API_MODULE}/activityVote/play_info`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-新品投票-新品投票信息预览
   *
   * @param {*} params
   * @returns
   */
  activityVotePreview(params) {
    return axios({
      url: `${API_MODULE}/activityVote/preview`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-新品投票-获取新品投票详情
   *
   * @param {*} params
   * @returns
   */
  activityVoteDetails(params) {
    return axios({
      url: `${API_MODULE}/activityVote/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-新品投票-删除新品投票信息
   *
   * @param {*} params
   * @returns
   */
  activityVoteDelete(params) {
    return axios({
      url: `${API_MODULE}/activityVote/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 营销平台-新品投票-获取新品活动关联商品列表
   *
   * @param {*} params
   * @returns
   */
  activityVoteOffer(params) {
    return axios({
      url: `${API_MODULE}/activityVoteOffer`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-新品投票-新增新品活动关联商品
   *
   * @param {*} params
   * @returns
   */
  activityVoteOfferAdd(params) {
    return axios({
      url: `${API_MODULE}/activityVoteOffer`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-新品投票-编辑新品活动关联商品
   *
   * @param {*} params
   * @returns
   */
  activityVoteOfferEdit(params) {
    return axios({
      url: `${API_MODULE}/activityVoteOffer`,
      method: 'PUT',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-新品投票-获取新品活动关联商品详情
   *
   * @param {*} params
   * @returns
   */
  activityVoteOfferDetails(params) {
    return axios({
      url: `${API_MODULE}/activityVoteOffer/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-新品投票-删除新品活动关联商品信息
   *
   * @param {*} params
   * @returns
   */
  activityVoteOfferDelete(params) {
    return axios({
      url: `${API_MODULE}/activityVoteOffer/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 营销平台-新品投票-获取新品投票活动审核分页信息
   *
   * @param {*} params
   * @returns
   */
  activityVoteLogList(params) {
    return axios({
      url: `${API_MODULE}/activityVoteLog`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-新品投票-新品投票活动审核
   *
   * @param {*} params
   * @returns
   */
  activityVoteLog(params) {
    return axios({
      url: `${API_MODULE}/activityVoteLog`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-买家秀-获取买家秀活动列表
   *
   * @param {*} params
   * @returns
   */
  showList(params) {
    return axios({
      url: `${API_MODULE}/show`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-买家秀-新增买家秀活动
   *
   * @param {*} params
   * @returns
   */
  showAdd(params) {
    return axios({
      url: `${API_MODULE}/show`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-买家秀-编辑买家秀活动
   *
   * @param {*} params
   * @returns
   */
  showEdit(params) {
    return axios({
      url: `${API_MODULE}/show`,
      method: 'PUT',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-买家秀-获取买家秀活动审核列表
   *
   * @param {*} params
   * @returns
   */
  showLogList(params) {
    return axios({
      url: `${API_MODULE}/show/log`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-买家秀-批量审核买家秀活动
   *
   * @param {*} params
   * @returns
   */
  showLogBatch(params) {
    return axios({
      url: `${API_MODULE}/show/log`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 营销平台-买家秀-审核买家秀活动
   *
   * @param {*} params
   * @returns
   */
  showLog(params) {
    return axios({
      url: `${API_MODULE}/show`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 营销平台-买家秀-获取审核买家秀活动详情
   *
   * @param {*} params
   * @returns
   */
  showDetails(params) {
    return axios({
      url: `${API_MODULE}/show/log/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-买家秀-下线买家秀活动
   *
   * @param {*} params
   * @returns
   */
  showOffline(params) {
    return axios({
      url: `${API_MODULE}/show/offline/${params}`,
      method: 'PUT',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-买家秀-获取买家秀支出列表
   *
   * @param {*} params
   * @returns
   */
  showPayList(params) {
    return axios({
      url: `${API_MODULE}/show/pay`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-买家秀-获取支出记录的合计支出
   *
   * @param {*} params
   * @returns
   */
  showPaysta(params) {
    return axios({
      url: `${API_MODULE}/show/paysta`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-买家秀-买家秀信息预览
   *
   * @param {*} params
   * @returns
   */
  showPreview(params) {
    return axios({
      url: `${API_MODULE}/show/preview`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 营销平台-买家秀-买家秀活动统计接口
   *
   * @param {*} params
   * @returns
   */
  showSta(params) {
    return axios({
      url: `${API_MODULE}/show/sta`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 营销平台-买家秀-获取买家秀活动详情
   *
   * @param {*} params
   * @returns
   */
  showDetailsId(params) {
    return axios({
      url: `${API_MODULE}/show/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 营销平台-买家秀-删除买家秀信息
   *
   * @param {*} params
   * @returns
   */
  showDelete(params) {
    return axios({
      url: `${API_MODULE}/show/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 获取个人号列表(发朋友圈的)
   */
  // getPersonalNoListCircle(params) {
  //   return axios({
  //     url: `${API_MODULE}/wechat/account/data/friend_circle`,
  //     method: 'GET',
  //     params: params
  //   })
  // },
  // 获取发朋友圈个人号列表
  getPersonalNoListCircle(params) {
    return axios({
      url: '/common/v1.0/search/department/staff/account',
      method: 'GET',
      params: params
    })
  },
  // 所属个人号筛选
  getSelectPersonId(params) {
    return axios({
      url: `${API_MODULE}/wechat/account/data/wx_account2`,
      method: 'GET',
      params: params
    })
  }
}
