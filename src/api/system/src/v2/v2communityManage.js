import axios from 'axios'
const API_MODULE = `/manager/v2.1`
const removeEmpty = true
export default {
    /**
     * 社群管理-引流-自动通过-获取群自动通过好友记录表分页信息
     *
     * @param {*} params
     * @returns
     */
    getAutoPassList(params) {
        return axios({
            url: `${API_MODULE}/groupPassUserLog`,
            method: 'GET',
            removeEmpty: true,
            params: params
        })
    },
    /**
     * 社群管理-引流-自动通过-勾选自动通过
     *
     * @param {*} params
     * @returns
     */
    checkAutoPass(params) {
        return axios({
            url: `${API_MODULE}/groupPassUserLog/auto_pass`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 社群管理-引流-自动通过-根据企业id获取企业自动通过配置
     *
     * @param {*} params
     * @returns
     */
    getAutoPassConfig() {
        return axios({
            url: `${API_MODULE}/groupPassUserLog/enterprise_config`,
            method: 'GET'
        })
    },
    /**
     * 社群管理-引流-自动通过-编辑自动通过好友记录表-手动通过
     *
     * @param {*} params
     * @returns
     */
    setAutoPass(id) {
        return axios({
            url: `${API_MODULE}/groupPassUserLog/status/${id}`,
            method: 'PUT'
        })
    },
    /**
     * 社群管理-引流-自动通过-根据自动通过记录id获取被邀请人信息
     *
     * @param {*} params
     * @returns
     */
    getInviteeList(id) {
        return axios({
            url: `${API_MODULE}/groupPassUserLog/invitee/${id}`,
            method: 'GET'
        })
    },
    /**
     * 社群管理-统计-群统计-导出群统计数据
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupCountDownFile(params) {
        return axios({
            url: `${API_MODULE}/groupCount/export`,
            method: 'GET',
            responseType: 'blob',
            params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-统计-群统计-群分布-获取群分布信息
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupDistribute(params) {
        return axios({
            url: `${API_MODULE}/groupCount/distribute`,
            method: 'GET',
            params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-统计-互动统计-群互动统计列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    groupInteractiveMessageCount(params) {
        return axios({
            url: `${API_MODULE}/groupMessageCount`,
            method: 'GET',
            params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-群公告-获取微信群公告列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupNoticeList(params) {
        return axios({
            url: `${API_MODULE}/groupNotice`,
            method: 'GET',
            params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-群公告-群公告修改记录-获取群公告修改记录表列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupNoticeLog(params) {
        return axios({
            url: `${API_MODULE}/groupNoticeLog`,
            method: 'GET',
            params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-群公告-群公告修改记录-取消执行修改群公告
     * @param {*} params
     * @return {[type]}        [description]
     */
    cancelGroupNoticeLog(id) {
        return axios({
            url: `${API_MODULE}/groupNoticeLog/${id}`,
            method: 'PUT'
        })
    },
    /**
     * 社群管理-管理-群公告-群公告修改记录-获取群公告修改记录明细表列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupNoticeLogDetail(params) {
        return axios({
            url: `${API_MODULE}/groupNoticeLogDetail`,
            method: 'GET',
            params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-群公告-群公告修改记录-重新执行修改群公告
     * @param {*} params
     * @return {[type]}        [description]
     */
    reExecuteGroupNoticeLogDetail(id) {
        return axios({
            url: `${API_MODULE}/groupNoticeLogDetail/${id}`,
            method: 'PUT'
        })
    },
    /**
     * 社群管理-管理-群公告-编辑群公告
     * @param {*} params
     * @return {[type]}        [description]
     */
    saveGroupNotice(data) {
        return axios({
            url: `${API_MODULE}/groupNotice`,
            method: 'PUT',
            data,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-统计-群统计-群统计列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupCountList(params) {
        return axios({
            url: `${API_MODULE}/groupCount`,
            method: 'GET',
            params,
            removeEmpty: true
        })
    },
    /**
     * 群活动-获取群活动列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGActivityRecord(params) {
        return axios({
            url: `${API_MODULE}/activity`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群活动-获取群活动列表-统计总群数等
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGActivityStatics(params) {
        return axios({
            url: `${API_MODULE}/activity/allcount`,
            method: 'GET'
        })
    },
    /**
     * 群活动-新增群活动
     * @param {*} params
     * @return {[type]}        [description]
     */
    addGActivity(params) {
        return axios({
            url: `${API_MODULE}/activity`,
            method: 'POST',
            data: params
        })
    },
    /**
     * 群活动-编辑群活动
     * @param {*} params
     * @return {[type]}        [description]
     */
    editGActivity(params) {
        return axios({
            url: `${API_MODULE}/activity`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 群活动-群码禁用启用
     * @param {*} params
     * @return {[type]}        [description]
     */
    toggleStatusGActivity(params) {
        return axios({
            url: `${API_MODULE}/activity/disable`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 群活动-群人数设置
     * @param {*} params
     * @return {[type]}        [description]
     */
    setGroupNumGActivity(params) {
        return axios({
            url: `${API_MODULE}/activity/number`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 群活动-群活动统计
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGActivityStaticsLine(params) {
        return axios({
            url: `${API_MODULE}/activity/sta`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群活动-群活动统计(列表)
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGActivityStaticsTable(params) {
        return axios({
            url: `${API_MODULE}/activity/stalist`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群活动-获取页面配置详情
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGActivityConfigDetail(params) {
        return axios({
            url: `${API_MODULE}/activity/${params}`,
            method: 'GET'
        })
    },
    /**
     * 群活动-获取页面配置详情--保存
     * @param {*} params
     * @return {[type]}        [description]
     */
    saveGActivityConfigDetail(params) {
        return axios({
            url: `${API_MODULE}/activity/pageConfig`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 群活动-获取页面配置详情--预览
     * @param {*} params
     * @return {[type]}        [description]
     */
    previewGActivityConfigDetail(params) {
        return axios({
            url: `${API_MODULE}/group_activity_h5/pageConfig/preview`,
            method: 'POST',
            data: params
        })
    },
    /**
     * 群列表-统计总群数等
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListStatistics(params) {
        return axios({
            url: `${API_MODULE}/activity/list/allcount/${params}`,
            method: 'GET'
        })
    },
    /**
     * 群列表-获取群列表的列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListRecords(params) {
        return axios({
            url: `${API_MODULE}/activity/list`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群列表-群列表统计
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGlistStaticsLine(params) {
        return axios({
            url: `${API_MODULE}/activity/list/sta`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群列表-群列表统计(列表)
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGlistStaticsTable(params) {
        return axios({
            url: `${API_MODULE}/activity/list/stalist`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群列表-获取添加群群列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getAddGroupList(params) {
        return axios({
            url: `${API_MODULE}/activity/list/group`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群列表-添加群--tijiao
     * @param {*} params
     * @return {[type]}        [description]
     */
    addGroupList(params) {
        return axios({
            url: `${API_MODULE}/activity/list/group`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-点击活动群码
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListCodeInfo(params) {
        return axios({
            url: `${API_MODULE}/activity/list/code/${params}`,
            method: 'GET'
        })
    },
    /**
     * 群列表-手动上传二维码名片
     * @param {*} params
     * @return {[type]}        [description]
     */
    uploadGroupListCode(params) {
        return axios({
            url: `${API_MODULE}/activity/list`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-发手机指令上传二维码名片
     * @param {*} params
     * @return {[type]}        [description]
     */
    mobileUploadGroupListCode(params) {
        return axios({
            url: `${API_MODULE}/activity/list/upload`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-分配状态开启关闭
     * @param {*} params
     * @return {[type]}        [description]
     */
    toggleGroupListStatus(params) {
        return axios({
            url: `${API_MODULE}/activity/list/able`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-获取拉手详情
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListHandleDetail(params) {
        return axios({
            url: `${API_MODULE}/activity/list/pull/detail/${params}`,
            method: 'GET'
        })
    },
    /**
     * 群列表-获取拉手详情--获取个人号列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListHandlePersonList(params) {
        return axios({
            url: `${API_MODULE}/activity/list/user`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群列表-获取拉手详情--开启群拉手
     * @param {*} params
     * @return {[type]}        [description]
     */
    openGroupListHandle(params) {
        return axios({
            url: `${API_MODULE}/activity/list/start`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-获取拉手详情--更换拉手号
     * @param {*} params
     * @return {[type]}        [description]
     */
    changeGroupListHandle(params) {
        return axios({
            url: `${API_MODULE}/activity/list/change`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-移除拉手号
     * @param {*} params
     * @return {[type]}        [description]
     */
    removeGroupListHandle(params) {
        return axios({
            url: `${API_MODULE}/activity/list/remove`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 群列表-移出群聊
     * @param {*} params
     * @return {[type]}        [description]
     */
    removeGroupList(params) {
        return axios({
            url: `${API_MODULE}/activity/list/removegroup`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 群列表-点击群拉手
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListHandleClick(params) {
        return axios({
            url: `${API_MODULE}/activity/list/pull`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群列表-开启群拉手
     * @param {*} params
     * @return {[type]}        [description]
     */
    openGroupListHandleClick(params) {
        return axios({
            url: `${API_MODULE}/activity/list/start`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-关闭群拉手
     * @param {*} params
     * @return {[type]}        [description]
     */
    closeGroupListHandleClick(params) {
        return axios({
            url: `${API_MODULE}/activity/list/stop`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-暂停群拉手
     * @param {*} params
     * @return {[type]}        [description]
     */
    pauseGroupListHandleClick(params) {
        return axios({
            url: `${API_MODULE}/activity/list/suspend`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 群列表-获取群成员管理列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListManageRecord(params) {
        return axios({
            url: `${API_MODULE}/member`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 群列表-获取群成员管理列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGroupListManageInviteRecord(params) {
        return axios({
            url: `${API_MODULE}/member/invite`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 入群问候-获取入群问候全局表列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getGreetingGroupRecord(params) {
        return axios({
            url: `${API_MODULE}/groupEnterGreeting`,
            method: 'GET'
        })
    },
    /**
     * 入群问候-新增入群问候全局表
     * @param {*} params
     * @return {[type]}        [description]
     */
    addGreetingGroupRecord(params) {
        return axios({
            url: `${API_MODULE}/groupEnterGreeting`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 入群问候-编辑入群问候全局表
     * @param {*} params
     * @return {[type]}        [description]
     */
    modifyGreetingGroupRecord(params) {
        return axios({
            url: `${API_MODULE}/groupEnterGreeting`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 入群问候-删除入群问候全局表信息
     * @param {*} params
     * @return {[type]}        [description]
     */
    delGreetingGroupRecord(params) {
        return axios({
            url: `${API_MODULE}/groupEnterGreeting/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 入群问候-入群问候全局表上移下移
     * @param {*} params
     * @return {[type]}        [description]
     */
    sortGreetingGroupRecord(params) {
        return axios({
            url: `${API_MODULE}/groupEnterGreeting/sort`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 自动回复-获取群自动回复规则表列表
     * @param {*} params
     * @return {[type]}        [description]
     */
    getAutoRecoveryRecords(params) {
        return axios({
            url: `${API_MODULE}/groupAutoReplyRule`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 自动回复-删除群自动回复规则表信息
     * @param {*} params
     * @return {[type]}        [description]
     */
    delAutoRecoveryRecords(params) {
        return axios({
            url: `${API_MODULE}/groupAutoReplyRule/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 自动回复-获取群自动回复规则表详情
     * @param {*} params
     * @return {[type]}        [description]
     */
    getAutoRecoveryRecordsDetail(params) {
        return axios({
            url: `${API_MODULE}/groupAutoReplyRule/${params}`,
            method: 'GET'
        })
    },
    /**
     * 自动回复-新增群自动回复规则表
     * @param {*} params
     * @return {[type]}        [description]
     */
    addAutoRecoveryRecordsDetail(params) {
        return axios({
            url: `${API_MODULE}/groupAutoReplyRule`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 自动回复-编辑群自动回复规则表
     * @param {*} params
     * @return {[type]}        [description]
     */
    modifyAutoRecoveryRecordsDetail(params) {
        return axios({
            url: `${API_MODULE}/groupAutoReplyRule`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 社群管理-管理-群管理-群管理列表
     *
     * @param {*} params
     * @returns
     */
    communityList(params) {
        return axios({
            url: `${API_MODULE}/community`,
            method: 'GET',
            removeEmpty: true,
            params: params
        })
    },
    /**
     * 社群管理-管理-群管理-行为管理设置
     *
     * @param {*} params
     * @returns
     */
    communityAction(params) {
        return axios({
            url: `${API_MODULE}/community/action`,
            method: 'PUT',
            removeEmpty: true,
            data: params
        })
    },
    /**
     * 社群管理-管理-群管理-编辑群行为自定义配置表
     *
     * @param {*} params
     * @returns
     */
    communityActionCustom(params) {
        return axios({
            url: `${API_MODULE}/community/action_custom`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 社群管理-管理-群管理-获取群行为自定义列表
     *
     * @param {*} params
     * @returns
     */
    communityActionCustomId(params) {
        return axios({
            url: `${API_MODULE}/community/action_custom/${params}`,
            method: 'GET',
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-群管理-自动回复设置
     *
     * @param {*} params
     * @returns
     */
    communityAautoReply(params) {
        return axios({
            url: `${API_MODULE}/community/auto_reply`,
            method: 'PUT',
            removeEmpty: true,
            data: params
        })
    },
    /**
     * 社群管理-管理-群管理-入群问候设置
     *
     * @param {*} params
     * @returns
     */
    communityGreeting(params) {
        return axios({
            url: `${API_MODULE}/community/greeting`,
            method: 'PUT',
            removeEmpty: true,
            data: params
        })
    },
    /**
     * 社群管理-管理-群管理-获取入群设置自定义列表
     *
     * @param {*} params
     * @returns
     */
    communitygroupRelaEnterGreeting(params) {
        return axios({
            url: `${API_MODULE}/community/group_rela_enter_greeting/${params}`,
            method: 'GET',
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-群管理-群成员列表
     *
     * @param {*} params
     * @returns
     */
    communityMemberId(params) {
        return axios({
            url: `${API_MODULE}/community/member/${params}`,
            method: 'GET',
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-群管理-编辑群备注
     *
     * @param {*} params
     * @returns
     */
    communityRemark(params) {
        return axios({
            url: `${API_MODULE}/community/remark`,
            method: 'PUT',
            removeEmpty: true,
            data: params
        })
    },
    /**
     * 社群管理-管理-群管理-群设置修改
     *
     * @param {*} params
     * @returns
     */
    communitySetting(params) {
        return axios({
            url: `${API_MODULE}/community/setting`,
            method: 'PUT',
            removeEmpty: true,
            data: params
        })
    },
    /**
     * 社群管理-管理-群管理-个人号列表
     *
     * @param {*} params
     * @returns
     */
    communityWechatAccount(params) {
        return axios({
            url: `${API_MODULE}/community/wechat_account`,
            method: 'GET',
            removeEmpty: true,
            params: params
        })
    },
    /**
     * 社群管理-管理-群管理-设置个人号
     *
     * @param {*} params
     * @returns
     */
    communityWechatAccountId(params) {
        return axios({
            url: `${API_MODULE}/community/wechat_account`,
            method: 'PUT',
            removeEmpty: true,
            data: params
        })
    },
    /**
     * 社群管理-管理-群管理-设置工作群
     *
     * @param {*} params
     * @returns
     */
    communityWechatGroupWork(params) {
        return axios({
            url: `${API_MODULE}/community/wechat_group_work`,
            method: 'PUT',
            removeEmpty: true,
            data: params
        })
    },
    /**
     * 社群管理-管理-行为管理-获取群行为全局配置表列表
     *
     * @param {*} params
     * @returns
     */
    groupActionList(params) {
        return axios({
            url: `${API_MODULE}/groupAction`,
            method: 'GET'
        })
    },
    /**
     * 社群管理-管理-行为管理-编辑群行为全局配置表
     *
     * @param {*} params
     * @returns
     */
    groupActionEdit(params) {
        return axios({
            url: `${API_MODULE}/groupAction`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 社群管理-管理-行为管理-获取违规行为记录表列表
     *
     * @param {*} params
     * @returns
     */
    groupIllegalActionList(params) {
        return axios({
            url: `${API_MODULE}/groupIllegalAction`,
            method: 'GET',
            params: params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-行为管理-踢人或警告
     *
     * @param {*} params
     * @returns
     */
    groupIllegalAction(params) {
        return axios({
            url: `${API_MODULE}/groupIllegalAction`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 社群管理-管理-行为管理-违规行为详情
     *
     * @param {*} params
     * @returns
     */
    groupIllegalActionDetails(params) {
        return axios({
            url: `${API_MODULE}/groupIllegalAction/${params}`,
            method: 'GET'
        })
    },
    /**
     * 社群管理-管理-行为管理-获取群黑名单记录表列表
     *
     * @param {*} params
     * @returns
     */
    groupBlackList(params) {
        return axios({
            url: `${API_MODULE}/groupBlackList`,
            method: 'GET',
            params: params,
            removeEmpty: true
        })
    },
    /**
     * 社群管理-管理-行为管理-移入全局黑名单
     *
     * @param {*} params
     * @returns
     */
    groupBlackListId(params) {
        return axios({
            url: `${API_MODULE}/groupBlackList/${params}`,
            method: 'PUT'
        })
    },
    /**
     * 社群管理-管理-行为管理-移除黑名单
     *
     * @param {*} params
     * @returns
     */
    groupBlackDelete(params) {
        return axios({
            url: `${API_MODULE}/groupBlackList/${params.type}/${params.id}`,
            method: 'DELETE'
        })
    },
    // 社群---单群统计
    getSingleGroupList(params) {
        return axios({
            url: '/manager/v1.0/groupSingleStatistics',
            method: 'GET',
            params: params,
            removeEmpty: true
        })
    },
    // 单群统计---导出
    exportSingleGroupList(params) {
        return axios({
            url: '/manager/v1.0/groupSingleStatistics/export',
            method: 'GET',
            params: params,
            responseType: 'blob',
            removeEmpty: true
        })
    }

}