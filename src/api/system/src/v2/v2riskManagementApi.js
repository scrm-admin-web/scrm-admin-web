import axios from 'axios'
const API_MODULE = `/manager/v2.0`

export default {
  /**
   * 敏感词设置-敏感词分组列表-select
   *
   * @param {*} params
   * @returns
   */
  wordList(params) {
    return axios({
      url: `${API_MODULE}/risk_word_group/list`,
      method: 'GET'
    })
  },
  /**
   * 敏感词设置-敏感词分组列表
   *
   * @param {*} params
   * @returns
   */
  riskwordList(params) {
    return axios({
      url: `${API_MODULE}/risk_word_group`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 敏感词设置-敏感词分组列表--新增
   *
   * @param {*} params
   * @returns
   */
  riskwordListAdd(params) {
    return axios({
      url: `${API_MODULE}/risk_word_group`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 敏感词设置-敏感词分组列表--编辑
   *
   * @param {*} params
   * @returns
   */
  riskwordListModify(params) {
    return axios({
      url: `${API_MODULE}/risk_word_group`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 敏感词设置-敏感词分组列表--批量修改
   *
   * @param {*} params
   * @returns
   */
  riskwordListUpdate(params) {
    return axios({
      url: `${API_MODULE}/risk_word_group/list`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 敏感词设置-敏感词分组列表--删除
   *
   * @param {*} params
   * @returns
   */
  riskwordListDelete(params) {
    return axios({
      url: `${API_MODULE}/risk_word_group/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 敏感词设置-获取片区
   *
   * @param {*} params
   * @returns
   */
  // getArea(params) {
  //   return axios({
  //     url: `${API_MODULE}/risk_word_group/deptlist`,
  //     method: 'GET'
  //   })
  // },
  // 获取坐席id
  getStaffId(params) {
    return axios({
      url: '/common/v1.0/search/department/staff',
      method: 'GET'
    })
  }
}
