import axios from 'axios'
const API_MODULE = `/manager/v1.0/`

export default {
  /**
   * 手机号加好友-获取手机号加好友日志
   *
   * @param {*} data
   * @returns
   */
  fansList(data) {
    return axios({
      url: `${API_MODULE}phone/add/fans`,
      method: 'GET',
      removeEmpty: true,
      params: data
    })
  },
  /**
   * 手机号加好友-新增手机号加好友任务
   *
   * @param {*} data
   * @returns
   */
  addFans(data) {
    return axios({
      url: `${API_MODULE}phone/add/fans`,
      method: 'POST',
      data: data
    })
  },
  /**
   * 定时加好友-获取定时加好友任务列表
   *
   * @param {*} data
   * @returns
   */
  timeList(data) {
    return axios({
      url: `${API_MODULE}time/task/add/fans`,
      method: 'GET',
      removeEmpty: true,
      params: data
    })
  },
  /**
   * 定时加好友-通过文本框获取的数据，新增定时加好友任务
   *
   * @param {*} data
   * @returns
   */
  addByFormImport(data) {
    return axios({
      url: `${API_MODULE}time/task/add/fans/add_by_form_import`,
      method: 'POST',
      data: data
    })
  },
  /**
   * 定时加好友-定时加好友任务导入excel返回对应数据
   *
   * @param {*} data
   * @returns
   */
  importExcel(data) {
    return axios({
      url: `${API_MODULE}time/task/add/fans/import/excel`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: data
    })
  },
  /**
   * 定时加好友-通过导入excel文件获取的数据，新增定时加好友任务
   *
   * @param {*} data
   * @returns
   */
  addByExcelImport(data) {
    return axios({
      url: `${API_MODULE}time/task/add/fans/add_by_excel_import`,
      method: 'POST',
      data: data
    })
  },
  /**
   * 定时加好友-获取定时加好友日志列表
   *
   * @param {*} data
   * @returns
   */
  logsList(data) {
    return axios({
      url: `${API_MODULE}time/task/add/fans/logs`,
      method: 'GET',
      removeEmpty: true,
      params: data
    })
  },
  /**
   * 定时加好友-暂停/启动定时加好友任务
   *
   * @param {*} data
   * @returns
   */
  startOrPause(data) {
    return axios({
      url: `${API_MODULE}time/task/add/fans/start_or_pause`,
      method: 'PUT',
      data: data
    })
  },
  /**
   * 定时加好友-删除定时加好友任务信息
   *
   * @param {*} data
   * @returns
   */
  fansDelete(data) {
    return axios({
      url: `${API_MODULE}time/task/add/fans/${data}`,
      method: 'DELETE'
    })
  },
  /**
   * 店铺自动加好友-获取店铺定时加好友日志
   *
   * @param {*} data
   * @returns
   */
  logList(data) {
    return axios({
      url: `${API_MODULE}shop/add/fans/log`,
      method: 'GET',
      removeEmpty: true,
      params: data
    })
  },
  /**
   * 店铺自动加好友-获取店铺定时加好友配置
   *
   * @param {*} data
   * @returns
   */
  fansSetList(data) {
    return axios({
      url: `${API_MODULE}shop/add/fans`,
      method: 'GET'
    })
  },
  /**
   * 店铺自动加好友-修改店铺定时加好友配置
   *
   * @param {*} data
   * @returns
   */
  addShopFans(data) {
    return axios({
      url: `${API_MODULE}shop/add/fans`,
      method: 'PUT',
      data: data
    })
  },
  // 微活码
  /**
   * 微活码获取列表
   */
  getMicroCodeList(params) {
    return axios({
      url: `${API_MODULE}micro_code`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 新增微活码
   */
  newMicroCode(params) {
    return axios({
      url: `${API_MODULE}micro_code`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 编辑微活码
   */
  editMicroCode(params) {
    return axios({
      url: `${API_MODULE}micro_code`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 删除微活码
   */
  delMicroCode(id) {
    return axios({
      url: `${API_MODULE}micro_code/${id}`,
      method: 'DELETE'
    })
  },
  /**
   * 通过id获取微活码详情信息
   */
  getMicroCodeDtail(id) {
    return axios({
      url: `${API_MODULE}micro_code/${id}`,
      method: 'GET'
    })
  },
  /**
   * 获取个人号列表
   */
  getPersonalNoList(params) {
    return axios({
      url: `${API_MODULE}micro_code/person_info`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 删除微活码个人号
   */
  delPersonalNo(id) {
    return axios({
      url: `${API_MODULE}/micro_code/micro_wechat/${id}`,
      method: 'DELETE'
    })
  }
}
