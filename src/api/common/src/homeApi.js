import axios from 'axios'

export default {
  /**
   * 获取密码加密因子
   *
   * @returns
   */
  getEncryption() {
    return axios({
      url: '/common/v1.0/home/encryption',
      method: 'GET',
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 企业/员工登录
   *
   * @param {*} data
   * @returns
   */
  login(data) {
    return axios({
      url: '/common/v1.0/home/login',
      method: 'POST',
      data,
      loading: false
    })
  },
  /**
   * 获取当前用户信息
   *
   * @returns
   */
  getCurrent(loading = true) {
    return axios({
      url: '/common/v1.0/home/user',
      method: 'GET',
      responseType: 'json',
      errorHandle: false,
      loading
    })
  },
  /**
   * 登出
   */
  logout() {
    localStorage.removeItem('isXunFeiUser')
    return new Promise((resolve, reject) => {
      axios({
        url: '/common/v1.0/home/logout',
        method: 'GET',
        errorHandle: false
      }).then(() => resolve(), error => {
        if (error.response.status === 401) {
          resolve()
        } else {
          reject(error)
        }
      })
    })
  },
  /**
   * 获取系统配置
   */
  getSysConfig(key) {
    return axios({
      url: `/common/v1.0/home/sysconfig/${key}`,
      method: 'GET'
    })
  },
  /**
   * 修改密码
   *
   * @param {*} data
   * @returns
   */
  modifyPassword(data) {
    return axios({
      url: `/common/v1.0/home/password/modify`,
      method: 'POST',
      data
    })
  },
  /**
   * 贴牌商logo版权等信息
   *
   * @param {*} name
   * @returns
   */
  getOmsInfo(name) {
    return axios({
      url: `/common/v1.0/home/oem_info`,
      method: 'GET',
      params: {
        name
      }
    })
  }
}
