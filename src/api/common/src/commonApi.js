import axios from 'axios'
const API_MODULE_V = `/common/v1.0/`
const removeEmpty = true

export default {
  /**
   * 获取省份列表
   *
   * @param {*} params
   * @returns
   */
  provinceList(params) {
    return axios({
      url: `${API_MODULE_V}search/common/area/province`,
      method: 'GET'
    })
  },
  /**
   * 获取城市列表
   *
   * @param {*} params
   * @returns
   */
  cityList(params) {
    return axios({
      url: `${API_MODULE_V}search/common/area/city/${params}`,
      method: 'GET'
    })
  },
  /**
   * 获取省份列表
   *
   * @param {*} params
   * @returns
   */
  areaList(params) {
    return axios({
      url: `${API_MODULE_V}search/common/area/area/${params}`,
      method: 'GET'
    })
  },
  /**
   * 获取树形菜单
   *
   * @param {*} params
   * @returns
   */
  menuList(params) {
    return axios({
      url: `${API_MODULE_V}resource/menu/${params}`,
      method: 'GET'
    })
  },
  /**
   * 新增菜单
   *
   * @param {*} params
   * @returns
   */
  menuAdd(params) {
    return axios({
      url: `${API_MODULE_V}resource/add/menu`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 编辑菜单
   *
   * @param {*} params
   * @returns
   */
  menuEdit(params) {
    return axios({
      url: `${API_MODULE_V}resource/update/menu`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 删除菜单
   *
   * @param {*} params
   * @returns
   */
  menuDelete(params) {
    return axios({
      url: `${API_MODULE_V}resource/menu/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 移动菜单
   *
   * @param {*} params
   * @returns
   */
  menuMove(params) {
    return axios({
      url: `${API_MODULE_V}resource/move/menu/${params.child_id}/${params.parent_id}`,
      method: 'PUT'
    })
  },
  /**
   * 获取角色列表
   *
   * @param {*} params
   * @returns
   */
  roleList(params) {
    return axios({
      url: `${API_MODULE_V}/resource/role/${params}`,
      method: 'GET'
    })
  },
  /**
   * 获取数据权限
   *
   * @param {*} params
   * @returns
   */
  dataList(params) {
    return axios({
      url: `${API_MODULE_V}/resource/data/${params}`,
      method: 'GET'
    })
  },
  /**
   * 新增数据权限
   *
   * @param {*} params
   * @returns
   */
  dataAdd(params) {
    return axios({
      url: `${API_MODULE_V}/resource/add/data`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 修改数据权限
   *
   * @param {*} params
   * @returns
   */
  dataUpdate(params) {
    return axios({
      url: `${API_MODULE_V}/resource/update/data`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 删除数据权限
   *
   * @param {*} params
   * @returns
   */
  dataDelete(params) {
    return axios({
      url: `${API_MODULE_V}/resource/delete/data/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 同步资源
   *
   * @param {*} params
   * @returns
   */
  synchronous(params) {
    return axios({
      url: `${API_MODULE_V}/resource/synchronous/resource`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取功能权限
   *
   * @param {*} params
   * @returns
   */
  actionList(params) {
    return axios({
      url: `${API_MODULE_V}/resource/action/${params}`,
      method: 'GET'
    })
  },
  /**
   * 新增功能权限
   *
   * @param {*} params
   * @returns
   */
  actionAdd(params) {
    return axios({
      url: `${API_MODULE_V}/resource/add/action`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 修改功能权限
   *
   * @param {*} params
   * @returns
   */
  actionUpdate(params) {
    return axios({
      url: `${API_MODULE_V}/resource/update/action`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 删除功能权限
   *
   * @param {*} params
   * @returns
   */
  actionDelete(params) {
    return axios({
      url: `${API_MODULE_V}/resource/delete/action/${params}`,
      method: 'DELETE'
    })
  }

}
