import axios from 'axios'

export default {
  /**
   * 获取七牛云token
   *
   * @returns
   */
  qiniutoken() {
    return axios({
      url: '/common/v1.0/img/qiniutoken',
      method: 'GET',
      responseType: 'json'
    })
  }
}
