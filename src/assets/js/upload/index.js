import {
  Upload
} from 'vma-vue-element'

/*
  property
    // 支持的文件类型后缀名，不配置则允许全部
    exts: [],
    // 上传文件大小限制，小于等于0为不限制，单位: MB
    singleFileSize: 0,
    // 上传文件的name
    filename: 'file',
    data: {},
    headers: {},
    withCredentials: false,
    // 上传文件指定的类型
    model: qiniuModel,
    onProgress() {},
    // 提示消息
    showMessage(message) {
      Message({
        message,
        type: 'warning',
        duration: 10000
      })
    }

  method
    static extendDefaultOptions(opts)
    upload(blobOrBase64)
 */

Upload.extendDefaultOptions({
  transformResponseData(data, blobOrFile) {
    if (!data.ext && blobOrFile && blobOrFile.type) {
      data.url = `${data.domain}/${data.key}?attname=${data.key}.${blobOrFile.type.replace(/^([^/]+)\//, '')}`
    }
    return data
  },
  singleFileSize: 10
})

export default Upload
