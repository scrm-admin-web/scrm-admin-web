import webim from '@static/js/txWebIM/sdk/webim.js'
import loginConfig from './config'
// import extend from 'extend'
import listeners from './listeners/listeners'
// import { chatLogApi } from '@/api/chat'
import {
  chatApi
} from '@/api/chat'
import {
  removeEmpty
} from 'vma-vue-assist/dist/static/js/utils'
import Logger from '@/utils/logger'
import {
  MESSAGE_SUB_TYPE
} from '@/constants/chat'
import {
  deepCopy
} from '@/utils'

// 监听连接状态回调变化事件
let onConnNotify = function (resp) {
  let info
  switch (resp.ErrorCode) {
    case webim.CONNECTION_STATUS.ON:
      webim.Log.warn('建立连接成功: ' + resp.ErrorInfo)
      TxWebIM.onConnNotifyCallback.connectStatusOn(resp.ErrorCode)
      break
    case webim.CONNECTION_STATUS.OFF:
      info = '连接已断开，无法收到新消息，请检查下你的网络是否正常: ' + resp.ErrorInfo
      // alert(info)
      webim.Log.warn(info)
      TxWebIM.onConnNotifyCallback.connectStatusOff(info)
      break
    case webim.CONNECTION_STATUS.RECONNECT:
      info = '连接状态恢复正常: ' + resp.ErrorInfo
      // alert(info)
      webim.Log.warn(info)
      TxWebIM.onConnNotifyCallback.connectStatusReconnect(info)
      break
    default:
      webim.Log.error('未知连接状态: =' + resp.ErrorInfo)
      TxWebIM.onConnNotifyCallback.connectStatusUnknow(resp.ErrorInfo)
      break
  }
}
listeners.onConnNotify = onConnNotify

/**
 * 监听新消息
 * @param  {[type]} newMsgList 新消息数组，结构为[Msg]
 * @return {[type]}            [description]
 */
let onMsgNotify = function (newMsgList) {
  // debugger
  Logger.info('onMsgNotify', newMsgList)
  let j, newMsg, subType
  for (j in newMsgList) {
    newMsg = newMsgList[j]
    // 消息子类型
    subType = newMsg.getSubType()
    let {
      data,
      type
    } = TxWebIM.getMsgContent(newMsg)
    let fromAccount = newMsg.getFromAccount()
    if (subType === webim.C2C_MSG_SUB_TYPE.COMMON) {
      // c2c消息
      TxWebIM.onC2CMsgNotify({
        data,
        imAccount: fromAccount,
        msgELementType: type
      })
    } else {
      // 群消息
      TxWebIM.onGroupMsgNotify({
        data,
        imAccount: fromAccount,
        msgELementType: type
      })
    }
  }
}
listeners.onMsgNotify = onMsgNotify

listeners.onKickedEventCall = function () {
  TxWebIM.onKickedEventCall()
}

/**
 * 重写连接状态改变的方法
 * @param {[type]} connectStatusOn        建立连接成功
 * @param {[type]} connectStatusOff       连接断开
 * @param {[type]} connectStatusReconnect 连接状态恢复正常
 * @param {[type]} connectStatusUnknow    连接状态未知
 */
let setOnConnNotifyCallback = function ({
  connectStatusOn,
  connectStatusOff,
  connectStatusReconnect,
  connectStatusUnknow
}) {
  TxWebIM.onConnNotifyCallback.connectStatusOn = connectStatusOn
  TxWebIM.onConnNotifyCallback.connectStatusOff = connectStatusOff
  TxWebIM.onConnNotifyCallback.connectStatusReconnect = connectStatusReconnect
  TxWebIM.onConnNotifyCallback.connectStatusUnknow = connectStatusUnknow
}

/**
 * 绑定监听新消息的最终回调
 * @param {[type]} onC2CMsgNotify   [description]
 * @param {[type]} onGroupMsgNotify [description]
 */
let setOnReceivedMsgCallback = function (onC2CMsgNotify, onGroupMsgNotify) {
  TxWebIM.onC2CMsgNotify = onC2CMsgNotify
  TxWebIM.onGroupMsgNotify = onGroupMsgNotify
}

let setListenerOptions = function (listenerOptions) {
  TxWebIM.onKickedEventCall = listenerOptions.onKickedEventCall
}

class TxWebIM {
  constructor(options) {
    this.extendOptions(options || {})
  }

  /**
   * 设置实例配置
   * @param  {[type]} options.loginConfig 登录用户信息对象
   * @param  {[type]} options.onC2CMsgNotify   监听c2c消息
   * @param  {[type]} options.onGroupMsgNotify   监听群组消息
   * @param  {[type]} options.onConnNotifyCallback   监听连接状态
   * @param  {[type]} options.listeners   监听对象
   * @param  {[type]} options.reqMsgCount 拉取消息条数
   * @return {[type]}                     [description]
   */
  extendOptions({
    loginInfo,
    listenersOptions,
    options,
    onC2CMsgNotify,
    onGroupMsgNotify,
    onConnNotifyCallback,
    reqMsgCount
  } = {}) {
    if (reqMsgCount > 0 && reqMsgCount <= this.maxReqMsgCount) {
      this.reqMsgCount = reqMsgCount
    } else {
      this.reqMsgCount = this.maxReqMsgCount
    }
    // listeners.onConnNotify = this.onConnNotify
    // listeners.onMsgNotify = this.onMsgNotify
    listeners.jsonpCallback = this.jsonpCallback
    // 绑定监听连接状态的回调
    setOnConnNotifyCallback(onConnNotifyCallback)
    setListenerOptions(listenersOptions)
    // 绑定监听新消息的最终回调
    // this.onC2CMsgNotify = onC2CMsgNotify
    // this.onGroupMsgNotify = onGroupMsgNotify
    setOnReceivedMsgCallback(onC2CMsgNotify, onGroupMsgNotify)
    this.loginConfig = {
      loginInfo: loginInfo || loginConfig.loginInfo,
      options: options || loginConfig.options,
      listeners
    }
  }

  // 实例上的配置
  // ********************

  loginConfig = null

  maxReqMsgCount = 15

  // 拉取消息条数
  reqMsgCount = 0

  // 存储各好友下一次拉取的c2c消息时间和消息Key
  getPrePageC2CHistroyMsgInfoMap = []

  static onConnNotifyCallback = {
    connectStatusOn: null,
    connectStatusOff: null,
    connectStatusReconnect: null,
    connectStatusUnknow: null
  }

  // onMsgNotify(newMsgList) {
  //   let j, newMsg, subType
  //   for (j in newMsgList) {
  //     newMsg = newMsgList[j]
  //     // 消息子类型
  //     subType = newMsg.getSubType()
  //     let { data, type } = TxWebIM.getMsgContent(newMsg)
  //     if (subType === webim.C2C_MSG_SUB_TYPE.COMMON) {
  //       // c2c消息
  //       TxWebIM.onC2CMsgNotify(data, type)
  //     } else {
  //       // 群消息
  //       TxWebIM.onGroupMsgNotify(data, type)
  //     }
  //   }
  // }

  // *********************

  /**
   * sdk登录
   * @param  {[type]} options.loginInfo [description]
   * @param  {[type]} options.listeners [description]
   * @param  {Object} options.options [description]
   * @return {[type]}                   [description]
   */
  webimLogin(options) {
    let that = this
    return new Promise((resolve, reject) => {
      let loginInfo = that.loginConfig.loginInfo
      let listeners = that.loginConfig.listeners
      let options = that.loginConfig.options
      webim.login(
        loginInfo,
        listeners,
        options,
        function (resp) {
          resolve(resp)
        },
        function (err) {
          // alert(err.ErrorInfo)
          reject(err)
        }
      )
    })
  }

  /**
   * 登出
   * @return {[type]} [description]
   */
  webimLogout() {
    return new Promise((resolve, reject) => {
      webim.logout(
        function (resp) {
          Logger.info('登出IM')
          resolve(resp)
        },
        function (err) {
          reject(err)
        }
      )
    })
  }

  /**
   * 接收到c2c消息
   * @param  {[type]} msgContent 消息内容
   */
  static onC2CMsgNotify(msgContent) {}
  /**
   * 接收到c2c消息
   * @param  {[type]} msgContent 消息内容
   */
  static onGroupMsgNotify(msgContent) {}

  /**
   * 被其它实例提下线
   * @return {[type]} [description]
   */
  static onKickedEventCall() {}

  /**
   * 解析新消息的内容
   * @param  {[type]} newMsg 消息对象msg
   */
  static getMsgContent(newMsg) {
    let msgElems = newMsg.getElems()
    let type, content, lastContent, msgCustomType
    msgElems.forEach((val) => {
      type = val.getType()
      content = val.getContent()
      // console.log(type, content)
      switch (type) {
        // 文本消息
        case webim.MSG_ELEMENT_TYPE.TEXT:
          lastContent = TxWebIM.onReceivedMsgTypeText(content, lastContent)
          msgCustomType = 'text'
          break
          // 表情消息
        case webim.MSG_ELEMENT_TYPE.FACE:
          lastContent = TxWebIM.onReceivedMsgTypeFace(content, lastContent)
          msgCustomType = 'face'
          break
          // 图片消息
        case webim.MSG_ELEMENT_TYPE.IMAGE:
          lastContent = TxWebIM.onReceivedMsgTypeImage(content, lastContent)
          msgCustomType = 'image'
          break
          // 语音消息
        case webim.MSG_ELEMENT_TYPE.SOUND:
          lastContent = TxWebIM.onReceivedMsgTypeSound(content, lastContent)
          msgCustomType = 'sound'
          break
          // 文件消息
        case webim.MSG_ELEMENT_TYPE.FILE:
          lastContent = TxWebIM.onReceivedMsgTypeFile(content, lastContent)
          msgCustomType = 'file'
          break
          // 位置消息
        case webim.MSG_ELEMENT_TYPE.LOCATION:
          lastContent = TxWebIM.onReceivedMsgTypeLocation(content, lastContent)
          msgCustomType = 'location'
          break
          // 自定义消息
        case webim.MSG_ELEMENT_TYPE.CUSTOM:
          lastContent = TxWebIM.onReceivedMsgTypeCustom(content, lastContent)
          msgCustomType = 'custom'
          break
          // 群提示消息（只有群聊天才会出现）
        case webim.MSG_ELEMENT_TYPE.GROUP_TIP:
          lastContent = TxWebIM.onReceivedMsgTypeGroupTip(content, lastContent)
          msgCustomType = 'groupTip'
          break
      }
    })
    return {
      data: lastContent,
      type: msgCustomType,
      fromUserData: null
    }
  }

  /**
   * 解析文本消息
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeText(content, lastContent) {
    lastContent += content.getText()
  }
  /**
   * 解析表情消息
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeFace(content, lastContent) {
    if (lastContent.constructor !== 'Array') {
      lastContent = []
    }
    let index = content.getIndex()
    let data = content.getData()
    lastContent.push({
      index,
      data
    })
    return lastContent
  }
  /**
   * 解析图片消息
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeImage(content, lastContent) {
    return content.ImageInfoArray
  }
  /**
   * 解析语音消息
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeSound(content, lastContent) {
    let data = {
      second: content.getSecond(),
      size: content.getSize(),
      url: content.getDownUrl()
    }
    return data
  }
  /**
   * 解析文件消息
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeFile(content, lastContent) {}
  /**
   * 解析位置消息
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeLocation(content, lastContent) {}
  /**
   * 解析自定义消息
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeCustom(content, lastContent) {
    let dataStr = content.getData()
    let data
    try {
      data = dataStr ? JSON.parse(dataStr) : null
    } catch (error) {
      data = {}
    }
    if (data && (data.msgType === MESSAGE_SUB_TYPE.VMA_FILE_ELEM || data.msgType === MESSAGE_SUB_TYPE.WX_BUSINESS_CARD_ELEM || data.msgType === MESSAGE_SUB_TYPE.VMA_SOUND_ELEM)) {
      data.content = JSON.parse(data.content)
    }
    // 系统通知
    if (data && data.noticeType) {
      try {
        data.notice = JSON.parse(data.notice)
      } catch (err) {}
    }
    return data
    // let desc = content.getDesc()
    // let ext = content.getExt()
    // let obj
    // switch (data.msgType) {
    //   // 文本消息
    //   case 'VmaTextElem':
    //    break
    //   // 语音消息
    //   case 'VmaSoundElem':
    //    break
    //   // 图片消息
    //   case 'VmaImageElem':
    //    break
    //   // 视频消息
    //   case 'VmaFileElem':
    //    break
    //   // 系统消息,与文本消息展示样式不同
    //   case 'VmaSystemElem':
    //    break
    //   // 微信分享名片消息
    //   case 'WxBusinessCardElem':
    //    break
    //   // 微信位置消息
    //   case 'WxLocationElem':
    //    break
    //   // 微信转账消息
    //   case 'WxTransferElem':
    //    break
    //   // 微信红包消息
    //   case 'WxRedPacketElem':
    //    break
    //   // 微信共享位置消息
    //   case 'WxShareLocationElem':
    //    break
    // }
    // return obj
  }
  /**
   * 解析群提示消息（只有群聊天才会出现）
   * @param  {Object} content 消息元素内容对象
   * @return {[type]}         [description]
   */
  static onReceivedMsgTypeGroupTip(content, lastContent) {}

  // IE9(含)以下浏览器用到的jsonp回调函数
  jsonpCallback(rspData) {
    webim.setJsonpLastRspData(rspData)
  }

  // static extendOptions({loginConfig, listeners}) {
  //   // extend(true, TxWebIM.loginConfig, loginConfig)
  //   // extend(true, TxWebIM.listeners, listeners)
  //   return TxWebIM
  // }

  /**
   * 获取最新的c2c历史消息
   */
  getLastC2CHistoryMsgs(options) {
    return chatApi.getChatHistoryMsgList(options, false).then(resp => {
      // return chatLogApi.getChatFriendLog(options, false).then(resp => {
      resp.records.forEach(val => {
        try {
          // 文件、名片、音频
          if (val.content && (val.msgType === 'VmaFileElem' || val.msgType === 'WxBusinessCardElem' || val.msgType === 'VmaSoundElem')) {
            val.content = JSON.parse(val.content)
          }
          // 系统通知
          if (val.noticeType) {
            val.notice = JSON.parse(val.notice)
          }
        } catch (error) {}
      })
      return resp
    })
  }

  getLastGroupHistoryMsgs(options) {
    return chatApi.getChatHistoryMsgList(options, false).then(resp => {
      // return chatLogApi.getChatGroupsLog(options, false).then(resp => {
      resp.records.forEach(val => {
        try {
          // 文件、名片、音频
          if (val.content && (val.msgType === MESSAGE_SUB_TYPE.VMA_FILE_ELEM || val.msgType === MESSAGE_SUB_TYPE.WX_BUSINESS_CARD_ELEM || val.msgType === MESSAGE_SUB_TYPE.VMA_SOUND_ELEM)) {
            val.content = JSON.parse(val.content)
          }
          // 系统通知
          if (val.noticeType) {
            val.notice = JSON.parse(val.notice)
          }
        } catch (error) {}
      })
      return resp
    })
  }

  /**
   * 获取历史消息记录
   * @param  {[type]} options [description]
   * @return {[type]}         [description]
   */
  // getLastHistoryMsgs(options) {
  //   return this.getLastHistoryMsgsFromProprietarySystems(options)
  //   // if (sessionType === 'c2c') {
  //   //   return this.getLastHistoryMsgsFromProprietarySystems({
  //   //     selToID,
  //   //     reqMsgCount
  //   //   })
  //   // }
  // }

  /**
   * 从自己私有的系统获取消息记录
   */
  // getLastHistoryMsgsFromProprietarySystems(options) {
  //   // let data = {
  //   //   current: 1,
  //   //   size: reqMsgCount
  //   // }
  //   // let params = Object.assign(data, options)
  //   // this.getHistroyMsgInfoMapFromProprietarySystems[selToID] = data
  //   return chatApi.getChatHistoryMsgList(options).then((resp) => {
  //     resp.records.forEach(val => {
  //       // 文件、名片、音频
  //       if (val.content && (val.msgType === 'VmaFileElem' || val.msgType === 'WxBusinessCardElem' || val.msgType === 'VmaSoundElem')) {
  //         val.content = JSON.parse(val.content)
  //       }
  //       // 系统通知
  //       if (val.noticeType) {
  //         val.notice = JSON.parse(val.notice)
  //       }
  //     })
  //     return resp
  //   })
  // }

  /**
   * 获取最新的c2c历史消息,用于切换好友聊天，重新拉取好友的聊天消息
   * @param  {String} options.selToID     好友帐号
   * @param  {Number} options.reqMsgCount 拉取消息条数
   * @param  {Number} options.lastMsgTime 最近的消息时间，即从这个时间点向前拉取历史消息,第一次拉取好友历史消息时，必须传0
   * @param  {String} options.msgKey      服务器返回的最近消息Key,用于下次向前拉取历史消息,第一次可以传空字符串
   * @return {[type]}                     [description]
   */
  // getLastC2CHistoryMsgs({
  //   selToID,
  //   reqMsgCount
  // }) {
  //   if (reqMsgCount > this.maxReqMsgCount) {
  //     this.reqMsgCount = this.maxReqMsgCount
  //   }
  //   // 第一次拉取好友历史消息时，必须传0
  //   let lastMsgTime = 0
  //   let msgKey = ''
  //   let options = {
  //     // 好友帐号
  //     Peer_Account: selToID,
  //     // 拉取消息条数
  //     MaxCnt: reqMsgCount > 0 ? reqMsgCount : this.reqMsgCount,
  //     // 最近的消息时间，即从这个时间点向前拉取历史消息
  //     LastMsgTime: lastMsgTime,
  //     MsgKey: msgKey
  //   }
  //   if (options.MaxCnt > this.maxReqMsgCount) {
  //     options.MaxCnt = 15
  //   }
  //   let that = this
  //   return new Promise((resolve, reject) => {
  //     webim.getC2CHistoryMsgs(
  //       options,
  //       function(resp) {
  //         // 是否还有历史消息可以拉取，1-表示没有，0-表示有
  //         let complete = resp.Complete

  //         // 保留服务器返回的最近消息时间和消息Key,用于下次向前拉取历史消息
  //         that.saveLastC2CHistoryMsgsKey(selToID, lastMsgTime, msgKey, complete)
  //         let j, newMsg
  //         let notReadMsgsList = []
  //         for (j in resp.MsgList) {
  //           newMsg = resp.MsgList[j]
  //           let { data } = TxWebIM.getMsgContent(newMsg)
  //           notReadMsgsList.push(data)
  //         }
  //         resolve(notReadMsgsList)
  //         // resolve(
  //         //   resp,
  //         //   {
  //         //     msgList: resp.MsgList,
  //         //     complete,
  //         //     lastMsgTime: resp.LastMsgTime,
  //         //     msgKey: resp.MsgKey
  //         //   }
  //         // )
  //       },
  //       function(error) {
  //         reject(error)
  //       }
  //     )
  //   })
  // }
  /**
   * 向上翻页，获取更早的好友历史消息
   * @param  {[type]} options.selToID     好友帐号
   * @param  {Number} options.reqMsgCount 拉取消息条数
   * @param  {Number} options.lastMsgTime 最近的消息时间，即从这个时间点向前拉取历史消息,第一次拉取好友历史消息时，必须传0
   * @param  {String} options.msgKey      服务器返回的最近消息Key,用于下次向前拉取历史消息,第一次可以传空字符串
   */
  getPrePageC2CHistoryMsgs({
    selToID,
    reqMsgCount,
    lastMsgTime,
    msgKey
  }) {
    let tempInfo = this.getPrePageC2CHistroyMsgsKey(selToID)
    if (tempInfo.complete) {
      return Promise.resolve([])
    }
    if (reqMsgCount > this.maxReqMsgCount) {
      this.reqMsgCount = this.maxReqMsgCount
    }
    if (!lastMsgTime || !msgKey) {
      // 获取下一次拉取的c2c消息时间和消息Key
      lastMsgTime = tempInfo.lastMsgTime
      msgKey = tempInfo.msgKey
    }
    let options = {
      // 好友帐号
      Peer_Account: selToID,
      // 拉取消息条数
      MaxCnt: reqMsgCount > 0 ? reqMsgCount : this.reqMsgCount,
      // 最近的消息时间，即从这个时间点向前拉取历史消息
      LastMsgTime: lastMsgTime,
      MsgKey: msgKey
    }
    let that = this
    return new Promise((resolve, reject) => {
      webim.getC2CHistoryMsgs(
        options,
        function (resp) {
          // 是否还有历史消息可以拉取，1-表示没有，0-表示有
          let complete = resp.Complete

          // 保留服务器返回的最近消息时间和消息Key,用于下次向前拉取历史消息
          that.saveLastC2CHistoryMsgsKey(selToID, lastMsgTime, msgKey, complete)
          let j, newMsg
          let notReadMsgsList = []
          for (j in resp.MsgList) {
            newMsg = resp.MsgList[j]
            let {
              data
            } = TxWebIM.getMsgContent(newMsg)
            notReadMsgsList.push(data)
          }
          resolve(notReadMsgsList)
          // resolve(
          //   resp,
          //   {
          //     msgList: resp.MsgList,
          //     complete,
          //     lastMsgTime: resp.LastMsgTime,
          //     msgKey: resp.MsgKey
          //   }
          // )
        },
        function (error) {
          reject(error)
        }
      )
    })
  }

  /**
   * 获取最近的c2c聊天消息，保留服务器返回的最近消息时间和消息Key,用于下次向前拉取历史消息
   * @param  {[type]} selToID     对方id
   * @param  {[type]} lastMsgTime 最后的时间
   * @param  {[type]} msgKey      消息key
   */
  saveLastC2CHistoryMsgsKey(selToID, lastMsgTime, msgKey, complete) {
    this.getPrePageC2CHistroyMsgInfoMap[selToID] = {
      lastMsgTime,
      msgKey,
      complete
    }
  }
  /**
   * 获取更早的c2c聊天消息
   * @param  {[type]} selToID [description]
   * @return {[type]}         [description]
   */
  getPrePageC2CHistroyMsgsKey(selToID) {
    return this.getPrePageC2CHistroyMsgInfoMap[selToID]
  }
  /**
   * 发送消息
   * @param  {[type]} options [description]
   * @return {[type]}         [description]
   */
  sendMsg({
    msgData,
    selToID,
    identifier,
    msgSubType
  }) {
    // debugger
    let newMsg = Object.assign({}, removeEmpty(msgData))
    if (newMsg && (newMsg.msgType === MESSAGE_SUB_TYPE.VMA_FILE_ELEM || newMsg.msgType === MESSAGE_SUB_TYPE.WX_BUSINESS_CARD_ELEM || newMsg.msgType === MESSAGE_SUB_TYPE.VMA_SOUND_ELEM)) {
      newMsg = deepCopy(newMsg)
      newMsg.content = JSON.stringify(newMsg.content)
    } else if (newMsg && (newMsg.msgType === MESSAGE_SUB_TYPE.VMA_GROUP_SEND_ELEM)) {
      // 群发消息
      newMsg = deepCopy(newMsg)
      newMsg.messageList.forEach(v => {
        if (v && (v.msgType === MESSAGE_SUB_TYPE.VMA_VIDEO_ELEM || v.msgType === MESSAGE_SUB_TYPE.VMA_SOUND_ELEM)) {
          v.content = JSON.stringify(v.content)
        }
      })
      Logger.info('群发', newMsg)
    }
    if (typeof (newMsg.content) === 'string') {
      newMsg.content = newMsg.content.replace(/<br\/>/g, '\n').replace(/&nbsp;/ig, ' ')
    }
    newMsg = JSON.stringify(newMsg)
    Logger.info('发送-last', newMsg)
    // debugger
    switch (msgSubType) {
      case 'custom':
        return this.onSendC2cCustomMsg({
          msgContent: newMsg,
          selToID,
          identifier
        })
    }
  }
  /**
   * 发送c2c自定义消息
   * @param  {[type]} options.msgContent [description]
   * @param  {[type]} options.selToID    [description]
   * @param  {[type]} options.selType    会话类型,私聊/群聊
   * @param  {[type]} options.subType    消息子类型
   * @param  {[type]} options.selSess    [description]
   * @param  {[type]} options.fHeadUrl   [description]
   * @return {[type]}                    [description]
   */
  onSendC2cCustomMsg({
    msgContent,
    desc,
    ext,
    selToID,
    identifier,
    identifierNick,
    selSess,
    fHeadUrl = null
  } = {}) {
    if (!msgContent) {
      return Promise.reject(new Error('消息不能为空'))
    }
    // let msgLen = webim.Tool.getStrBytes(msgContent)
    // let maxLen
    let selType = webim.SESSION_TYPE.C2C
    // maxLen = webim.MSG_MAX_LENGTH.C2C
    // if (selType === webim.SESSION_TYPE.C2C) {
    //   maxLen = webim.MSG_MAX_LENGTH.C2C
    //   errInfo = `消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)`
    // } else {
    //   maxLen = webim.MSG_MAX_LENGTH.GROUP
    //   errInfo = `消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)`
    // }
    // if (msgLen > maxLen) {
    //   let errInfo = `消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)`
    //   return Promise.reject(new Error(errInfo))
    // }
    if (!selSess) {
      selSess = new webim.Session(selType, selToID)
    }
    // 是否为自己发送
    let isSend = true
    // 消息序列，-1表示sdk自动生成，用于去重
    let seq = -1
    // 消息随机数，用于去重
    let random = Math.round(Math.random() * 4294967296)
    // 消息时间戳
    let msgTime = Math.round(new Date().getTime() / 1000)
    // let subType//消息子类型
    // if (selType === webim.SESSION_TYPE.C2C) {
    //   subType = webim.C2C_MSG_SUB_TYPE.COMMON
    // } else {
    //   // webim.GROUP_MSG_SUB_TYPE.COMMON-普通消息,
    //   // webim.GROUP_MSG_SUB_TYPE.LOVEMSG-点赞消息，优先级最低
    //   // webim.GROUP_MSG_SUB_TYPE.TIP-提示消息(不支持发送，用于区分群消息子类型)，
    //   // webim.GROUP_MSG_SUB_TYPE.REDPACKET-红包消息，优先级最高
    //   subType = webim.GROUP_MSG_SUB_TYPE.COMMON
    // }
    let subType = webim.C2C_MSG_SUB_TYPE.COMMON
    let msg = new webim.Msg(selSess, isSend, seq, random, msgTime, identifier, subType, identifierNick || identifier)
    let customObj = new webim.Msg.Elem.Custom(msgContent, desc, ext)
    msg.addCustom(customObj)
    // 解析表情
    // this.exprFaceAndText(msg, msgContent)
    return new Promise((resolve, reject) => {
      webim.sendMsg(msg, function (resp) {
        // 发送成功
        resolve(resp)
      }, function (err) {
        // 提示重发
        reject(err)
      })
    })
  }

  /**
   * 设置已读
   * @param {[type]}  options.selToID    [description]
   * @param {[type]}  options.selType    [description]
   * @param {Boolean} options.isAutoOn   将selSess的自动已读消息标志改为isOn，同时是否上报当前会话已读消息
   * @param {Boolean} options.isResetAll 是否重置所有会话的自动已读标志
   */
  setAlreadyRead({
    selToID,
    selType,
    isAutoOn = false,
    isResetAll = false
  }) {
    if (selType === 'c2c') {
      selType = webim.SESSION_TYPE.C2C
    } else {
      selType = webim.SESSION_TYPE.GROUP
    }
    let selSess = webim.MsgStore.sessByTypeId(selType, selToID)
    selSess && webim.setAutoRead(selSess, isAutoOn, isResetAll)
  }

  /**
   * 解析文本跟emoji表情
   * @param  {[type]} msg        消息对象
   * @param  {[type]} msgContent 输入的消息
   */
  exprFaceAndText(msg, msgContent) {
    let textObj, faceObj, tmsg, emotionIndex, emotion, restMsgIndex
    let expr = /\[[^[\]]{1,3}\]/mg
    let emotions = msgContent.match(expr)
    if (!emotions || emotions.length < 1) {
      textObj = new webim.Msg.Elem.Text(msgContent)
      msg.addText(textObj)
    } else {
      for (let i = 0; i < emotions.length; i++) {
        tmsg = msgContent.substring(0, msgContent.indexOf(emotions[i]))
        if (tmsg) {
          textObj = new webim.Msg.Elem.Text(tmsg)
          msg.addText(textObj)
        }
        emotionIndex = webim.EmotionDataIndexs[emotions[i]]
        emotion = webim.Emotions[emotionIndex]

        if (emotion) {
          faceObj = new webim.Msg.Elem.Face(emotionIndex, emotions[i])
          msg.addFace(faceObj)
        } else {
          textObj = new webim.Msg.Elem.Text(emotions[i])
          msg.addText(textObj)
        }
        restMsgIndex = msgContent.indexOf(emotions[i]) + emotions[i].length
        msgContent = msgContent.substring(restMsgIndex)
      }
      if (msgContent) {
        textObj = new webim.Msg.Elem.Text(msgContent)
        msg.addText(textObj)
      }
    }
  }

  /**
   * 获取c2c未读消息
   */
  getNotReadMsgs({
    sessionType,
    cbOk,
    cbErr
  }) {
    let j, newMsg
    let notReadMsgsList = []
    return webim.syncMsgs((newMsgList) => {
      for (j in newMsgList) {
        newMsg = newMsgList[j]
        let {
          data,
          type
        } = TxWebIM.getMsgContent(newMsg)
        let fromImAccount = newMsg.getFromAccount()
        notReadMsgsList.push({
          data,
          fromImAccount,
          type
        })
      }
      cbOk(notReadMsgsList)
    }, (errorInfo) => {
      cbErr(errorInfo)
    })
  }

  /**
   * 建立连接成功
   */
  // static connectStatusOn(errorInfo) {}
  /**
   * 连接断开
   */
  // connectStatusOff(errorInfo) {}
  /**
   * 连接状态恢复正常
   */
  // connectStatusReconnect(errorInfo) {}
  /**
   * 连接状态未知
   */
  // connectStatusUnknow(errorInfo) {}
}

export default TxWebIM
