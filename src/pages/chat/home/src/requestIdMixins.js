// import uuid from 'uuid'
import {
  v4 as uuidv4
} from 'uuid'
import {
  mapGetters,
  mapMutations
} from 'vuex'
import * as mutationsType from '@/store/mutation-types.js'
import {
  customerServiceApi
} from '@/api/chat'

export default {
  computed: {
    ...mapGetters([
      'activeChatFriendInfo',
      'chatUserInfoDB'
    ])
  },
  methods: {
    ...mapMutations({
      updateChatAccountInfo: mutationsType.ACTIVE_CHAT_ACCOUNT_INFO,
      updateChatFriendInfo: mutationsType.ACTIVE_CHAT_FRIEND_INFO
    }),
    /**
     * 更改当前好友信息
     * @param  {[type]}  data        要保存的好友信息
     * @param  {[type]}  accountInfo 好友对应的个人号
     * @param  {Boolean} refresh     是否从后台获取一次最新好友消息
     */
    async updateChatFriend(data, accountInfo, refresh = false) {
      // 好友被转接出去、非集中对话下，切换个人号
      if (!data) {
        return this.updateChatFriendInfo(data)
      }
      let oldInfo = this.activeChatFriendInfo
      // 设置id，更新为了防止短时间频繁来回切换好友、以及请求返回延时导致的数据重复问题
      if (oldInfo && oldInfo.accountId === data.accountId && oldInfo.type === data.type && oldInfo.wxId === data.wxId) {
        this.setChatRequestID(data, oldInfo.requestId)
      } else {
        this.setChatRequestID(data)
      }
      // 集中对话时
      if (!oldInfo || (this.concentratedDialogue && oldInfo.accountId !== data.accountId && accountInfo)) {
        this.updateChatAccountInfo(accountInfo)
      }
      this.updateChatFriendInfo(data)
      // 请求服务器用户信息，刷新一次本地存储
      refresh && this.refreshUserInfo(data)
    },
    // 请求服务器用户信息，刷新一次本地存储
    async refreshUserInfo(data) {
      if (!data.wxId) return
      let newUserInfos = await customerServiceApi.getPersonalAccountRecentChatsInfoListByIds({
        details: [{
          type: data.type,
          wxId: data.wxId
        }],
        id: data.accountId
      })
      let userInfo = newUserInfos[0]
      if (!userInfo.wxId) return
      // 用于更新列表中实时显示的信息
      this.setUserInfo(data.id, data.accountId, userInfo)
      // 更新indexedDB中的信息
      this.chatUserInfoDB.putUserInfo(userInfo)
    },
    // 用于更新列表中实时显示的信息
    setUserInfo(fId, accountId, newInfo) {},
    // 设置本次id
    setChatRequestID(data, requestId = null) {
      data.requestId = requestId || uuidv4()
      // data.requestId = requestId
    },
    // 获取本次id
    getChatRequestID() {
      return this.activeChatFriendInfo && this.activeChatFriendInfo.requestId
    }
  }
}
