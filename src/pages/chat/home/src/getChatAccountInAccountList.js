/**
 * 获取对应的个人号信息
 * @return {[type]} [description]
 */
export default ({accountList, imAccount, accountId}) => {
  if (!accountList || (accountList && accountList.length === 0)) {
    return {}
  }
  let accountInfo = accountList.find(val => {
    return val.imAccount === imAccount || val.id === accountId
  })
  return accountInfo
}
