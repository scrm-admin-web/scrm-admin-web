import {
  eachOwn
} from '@/utils'

const wxMomentEntity = {
  a: 'content',
  b: 'urls',
  c: 'blob'
}

const wxMomentLikeEntity = {
  a: 'wxId',
  b: 'nickName'
}

const wxMomentCommentEntity = {
  a: 'wxId',
  b: 'otherWxId',
  c: 'nickName',
  d: 'content',
  e: 'commentTime',
  f: 'commentArg',
  g: 'commentId1',
  h: 'commentId2'
}

const wxLocalMomentEntity = {
  a: 'snsId',
  b: 'type',
  c: 'createTime',
  d: {
    value: 'momentEntity',
    type: 'object',
    entity: wxMomentEntity
  },
  e: {
    value: 'likeList',
    type: 'array',
    entity: wxMomentLikeEntity
  },
  f: {
    value: 'commentList',
    type: 'array',
    entity: wxMomentCommentEntity
  }
}

const mappingObject = (item, obj) => {
  eachOwn(obj, (v, k) => {
    if (item.hasOwnProperty(k)) {
      if (v.value) {
        let val = item[k]
        switch (v.type) {
          case 'object':
            val = mappingObject(val, v.entity)
            break
          case 'array':
            val = mappingArray(val, v.entity)
            break
        }
        item[v.value] = val
      } else {
        item[v] = item[k]
      }
    }
  })
  return item
}

const mappingArray = (list, obj) => {
  return list.map(v => mappingObject(v, obj))
}

export default data => {
  if (Array.isArray(data)) {
    return mappingArray(data, wxLocalMomentEntity)
  } else {
    return mappingObject(data, wxLocalMomentEntity)
  }
}
