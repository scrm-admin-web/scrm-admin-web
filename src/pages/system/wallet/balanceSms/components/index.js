// import CodePay from './codePay'
import RechargeSms from './rechargeSms'
import rechargeMeal from './rechargeMeal'
import { CodePay } from '@/components/common/wallet'
export {
  CodePay, // 充值红包 => 二维码付款
  RechargeSms, // 短信营销 =>短信充值 => 充值
  rechargeMeal // 短信充值--选择套餐
}
