import dManageDialogAdd from './dManageDialogAdd'
import dManageDialogRemark from './dManageDialogRemark'
import dManageDialogSetGroup from './dManageDialogSetGroup'
import dManageDialogAppSet from './dManageDialogAppSet'
import dManageDialogRisk from './dManageDialogRisk'
import dManageDialogPreserver from './dManageDialogPreserver'
import dManageDialogImportConcats from './dManageDialogImportConcats'
import excelUpload from '@/components/common/excelUpload'
import dGroupAdd from './dGroupAdd'
import qReplayGroupAdd from './qReplayGroupAdd'
import qReplayMessageAdd from './qReplayMessageAdd'
import gReplayFriendsAutoTableList from './gReplayFriendsAutoTableList'
import gReplayMessageAutoTableList from './gReplayMessageAutoTableList'
import gReplayMessageAdd from './gReplayMessageAdd'
import gReplayMessageRuleAdd from './gReplayMessageRuleAdd'
import gReplayMessageRuleAddMsg from './gReplayMessageRuleAddMsg'
import pAutoReplayMessage from './pAutoReplayMessage'
import pAutoReplayAddMsg from './pAutoReplayAddMsg'
import mManageMessageAdd from './mManageMessageAdd'
import dManageDialogJournal from './dManageDialogJournal'

import dManageDialogDevicePer from './dManageDialogDevicePer'

export {
  dManageDialogAdd, // 设备管理 => 新增
  dManageDialogRemark, // 设备管理 => 修改备注
  dManageDialogSetGroup, // 设备管理 => 设置分组
  dManageDialogPreserver, // 设备管理 => 设备保管者
  dManageDialogRisk, // 设备管理 => 权限
  dManageDialogAppSet, // 设备管理 => App
  dManageDialogImportConcats, // 设备管理 => 导入联系人
  excelUpload, // 设备管理 => excel导入
  dGroupAdd, // 设备分组 => 新增
  qReplayGroupAdd, // 快捷回复 => 新增左侧快捷语分组
  qReplayMessageAdd, // 快捷回复 => 新增右侧快捷回复
  gReplayFriendsAutoTableList, // 全局自动回复 => 好友通过后自动回复table
  gReplayMessageAutoTableList, // 全局自动回复 => 新消息后自动回复table
  gReplayMessageAdd, // 全局自动回复 => 新增右侧消息
  gReplayMessageRuleAdd, // 全局自动回复 => 新增规则
  gReplayMessageRuleAddMsg, // 全局自动回复 => 新增规则 => 新增消息
  pAutoReplayMessage, // 个人号自动回复 => 设置自动回复
  pAutoReplayAddMsg, // 个人号自动回复 => 设置自动回复--新增消息
  mManageMessageAdd, // 素材管理 => 新增素材
  dManageDialogDevicePer, // 设备管理 => 历史使用人记录
  dManageDialogJournal // 设备管理 => 开启日志
}
