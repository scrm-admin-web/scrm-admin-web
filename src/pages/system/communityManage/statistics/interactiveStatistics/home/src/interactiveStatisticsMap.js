const INTERACTIVE_STATISTICS_MAP = {
  // 群消息数
  GROUP_MSG_NUM: {
    title: '群消息数',
    content: '当天群聊消息总数（不含系统消息）',
    key: 'groupMsgNum'
  },
  // 群发言人数
  GROUP_MSG_USER_NUM: {
    title: '群发言人数',
    content: '当天群聊中发言的人数',
    key: 'groupMsgUserNum'
  },
  // 群平均消息数
  GROUP_AVG_MSG_NUM: {
    title: '群平均消息数',
    content: '群消息数/群发言人数',
    key: 'groupAvgMsgNum'
  },
  // 群发言比例
  GROUP_MSG_PROPORTION: {
    title: '群发言比例',
    content: '当天群发言人数/群成员总数*100%',
    key: 'groupMsgProportion'
  }
}

export default INTERACTIVE_STATISTICS_MAP
