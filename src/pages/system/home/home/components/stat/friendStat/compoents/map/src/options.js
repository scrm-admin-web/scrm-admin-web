export default {
  title: {
    text: '好友地区统计',
    left: 'center',
    textStyle: {
      color: '#2897FF'
    }
  },
  tooltip: {
    trigger: 'item',
    showDelay: 0,
    transitionDuration: 0.2
  },
  visualMap: {
    min: 0,
    max: 60000,
    // max: 10000,
    itemWidth: 10,
    itemHeight: 180,
    inRange: {
      // color: ['#cbeaff', '#8dcffb', '#66c1ff', '#8accff', '#0089e7']
      color: ['#01d4e7', '#7c56c1', '#59aac8', '#16b18b', '#fbd062', '#eeb311', '#f5791e', '#d92c28']
    },
    text: ['高', '低'],
    calculable: true
  },
  series: [{
    name: '中国地图',
    type: 'map',
    roam: false,
    map: 'china',
    top: 20,
    right: 20,
    bottom: 20,
    left: 20,
    label: {
      normal: {
        show: true,
        color: '#454545'
      },
      emphasis: {
        show: true
      }
    },
    itemStyle: {
      normal: {
        borderWidth: 1,
        borderColor: '#fff'
      },
      emphasis: {
        borderWidth: 2,
        borderColor: '#fff',
        areaColor: '#fed183'
      }
    }
  }]
}
