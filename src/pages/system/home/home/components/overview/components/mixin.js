export default {
  props: {
    data: {
      type: Object,
      default () {
        return {}
      }
    }
  },
  methods: {
    refresh() {
      this.$emit('refresh')
    }
  }
}
