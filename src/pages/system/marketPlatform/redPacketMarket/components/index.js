import RechargeRedpacket from './rechargeRedpacket'
import { CodePay } from '@/components/common/wallet'

export {
  RechargeRedpacket, // 红包营销 => 充值红包
  CodePay // 充值红包 => 二维码付款
}
