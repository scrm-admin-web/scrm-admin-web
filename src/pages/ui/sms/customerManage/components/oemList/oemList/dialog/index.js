import accountAdd from './accountAdd'
import equipmentAdd from './equipmentAdd'
import shopAdd from './shopAdd'
import numAdd from './numAdd'
import resetPassword from './resetPassword'

export {
  accountAdd,
  equipmentAdd,
  shopAdd,
  numAdd,
  resetPassword
}
