export default {
  color: ['#44C89A', '#FEC870', '#1F9FF6'],
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)'
  },
  legend: {
    icon: 'circle',
    orient: 'vertical',
    // x: 'left',
    // y: 'bottom',
    itemWidth: 14,
    itemHeight: 14,
    left: 10,
    bottom: 12,
    data: ['男性', '女性', '未知']
  },
  grid: {
    containLabel: true
  },
  calculable: true,
  series: [{
    name: '统计结果',
    type: 'pie',
    radius: '60%',
    center: ['50%', '50%'],
    data: [
      {
        value: 335,
        name: '男性'
      },
      {
        value: 310,
        name: '女性'
      },
      {
        value: 234,
        name: '未知'
      }
    ]
  }]
}
