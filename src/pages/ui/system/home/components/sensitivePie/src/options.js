export default {
  title: {
    text: '敏感操作统计 共10(个)',
    x: 'center',
    textStyle: {
      color: '#2897FF'
    }
  },
  color: ['#1F9FF6', '#FEC870', '#FF786E'],
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)'
  },
  legend: {
    orient: 'vertical',
    x: 'left',
    y: 'bottom',
    data: ['打开微信拍照', '尝试登录', '敏感消息']
  },
  calculable: true,
  series: [{
    name: '统计结果',
    type: 'pie',
    radius: '60%',
    center: ['50%', '60%'],
    data: [
      {
        value: 335,
        name: '打开微信拍照'
      },
      {
        value: 310,
        name: '尝试登录'
      },
      {
        value: 234,
        name: '敏感消息'
      }
    ]
  }]
}
