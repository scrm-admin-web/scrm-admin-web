export default {
  title: {
    text: '好友性别统计 共10(位)',
    x: 'center',
    textStyle: {
      color: '#2897FF'
    }
  },
  color: ['#44C89A', '#FEC870', '#1F9FF6'],
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)'
  },
  legend: {
    orient: 'vertical',
    x: 'left',
    y: 'bottom',
    data: ['男性', '女性', '未知']
  },
  // toolbox: {
  //   show: true,
  //   feature: {
  //     mark: {
  //       show: true
  //     },
  //     magicType: {
  //       show: true,
  //       type: ['pie', 'funnel'],
  //       option: {
  //         funnel: {
  //           x: '25%',
  //           width: '50%',
  //           funnelAlign: 'left',
  //           max: 1548
  //         }
  //       }
  //     },
  //     saveAsImage: {
  //       show: true
  //     }
  //   }
  // },
  calculable: true,
  series: [{
    name: '统计结果',
    type: 'pie',
    radius: '60%',
    center: ['50%', '60%'],
    data: [
      {
        value: 335,
        name: '男性'
      },
      {
        value: 310,
        name: '女性'
      },
      {
        value: 234,
        name: '未知'
      }
    ]
  }]
}
