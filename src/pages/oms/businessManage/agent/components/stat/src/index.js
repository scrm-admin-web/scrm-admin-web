import basicInfo from '../components/basicInfo'
import enterpriseList from '../components/enterpriseList'
import equipmentDetails from '../components/equipmentDetails'
import authorizedShop from '../components/authorizedShop'
import personalNum from '../components/personalNum'

export {
  basicInfo,
  enterpriseList,
  equipmentDetails,
  authorizedShop,
  personalNum
}
