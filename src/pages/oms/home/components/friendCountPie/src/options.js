export default {
  color: ['#1F9FF6', '#44C89A', '#FEC870'],
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)'
  },
  legend: {
    icon: 'circle',
    orient: 'vertical',
    itemWidth: 8,
    itemHeight: 8,
    left: 10,
    bottom: 12,
    data: ['未知', '男', '女']
  },
  calculable: true,
  series: [{
    name: '统计结果',
    type: 'pie',
    radius: '50%',
    center: ['50%', '50%'],
    itemStyle: {
      normal: {
        label: {
          show: true,
          formatter: '{b} : {c} ({d}%)'
        },
        labelLine: {
          show: true
        }
      }
    },
    data: []
  }]
}
