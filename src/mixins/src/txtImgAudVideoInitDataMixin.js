export default {
  data() {
    return {}
  },
  created() {
    // cover_info:视频资源封面 msg_type: 1文本2图片3语音4视频 msg_text:消息内容 文本信息/资源url
    // 多个文件用逗号分隔 视频：封面与路径要一一对应
    if (this.dialogProp.data) {
      let modifyObj = {}
      modifyObj.msgType = this.dialogProp.data.msgType
      switch (this.dialogProp.data.msgType) {
        case (this.MESSAGE_TPYE_KEY.TXT):
          modifyObj.content = this.dialogProp.data.msgText
          break
        case (this.MESSAGE_TPYE_KEY.IMG):
          modifyObj.imgList = this.dialogProp.data.msgText.split(',')
          break
        case (this.MESSAGE_TPYE_KEY.AUDIO):
          modifyObj.audioList = this.dialogProp.data.msgText.split(',')
          // this.audioList = [this.dialogProp.data.msgText]
          break
        case (this.MESSAGE_TPYE_KEY.VIDEO):
          modifyObj.videoList = this.dialogProp.data.msgText.split(',')
          let coverInfoArr = this.dialogProp.data.coverInfo ? this.dialogProp.data.coverInfo.split(',') : []
          let posterArr = []
          for (let i = 0; i < modifyObj.videoList.length; i++) {
            let postObj = {
              img: coverInfoArr[i] || '',
              video: modifyObj.videoList[i]
            }
            posterArr.push(postObj)
          }
          modifyObj.posterArr = posterArr
          break
        case (this.MESSAGE_TPYE_KEY.WECHAT):
          modifyObj.weChatInfo = JSON.parse(this.dialogProp.data.msgText)
          // this.audioList = [this.dialogProp.data.msgText]
          break
        case (this.MESSAGE_TPYE_KEY.IMGTXTLINK):
          modifyObj.linkInfo = JSON.parse(this.dialogProp.data.msgText)
          break
      }
      this.modifyObj = modifyObj
    }
  },
  methods: {}
}
