export default {
  data() {
    return {
      checkedList: {
        homeList: [],
        tableList: [],
        divisionList: [],
        basicsList: []
      },
      sureList: []
    }
  },
  methods: {
    // 全选
    allElection(defObj, type, list, hierarchy, index, oneIndex) {
      var item = null
      switch (hierarchy) {
        case 1:
          return
        case 2:
          item = this.checkedList[list][index]
          break
        case 3:
          if (!oneIndex && oneIndex !== 0) {
            item = this.checkedList[list][index]
            for (let x = 0; x < item.node.length; x += 1) {
              item.node[x].check = defObj
              if (item.node[x].node) {
                var itemCld = item.node[x].node
                for (let y = 0; y < itemCld.length; y += 1) {
                  itemCld[y].check = defObj
                }
              }
            }
            return
          } else {
            item = this.checkedList[list][index].node[oneIndex]
          }
          break
      }
      if (item.node) {
        for (let x = 0; x < item.node.length; x += 1) {
          item.node[x].check = defObj
        }
      }
      if (defObj && item.node) {
        item.checkedNum = item.node.length
      } else {
        item.checkedNum = 0
      }
      this.allChecked(type, list, index)
    },
    // 单选
    changeElection(defObj, type, list, hierarchy, index, oneIndex, twoIndex) {
      var item = null
      switch (hierarchy) {
        case 1:
          return
        case 2:
          item = this.checkedList[list][index]
          break
        case 3:
          item = this.checkedList[list][index].node[oneIndex]
          break
      }
      if (defObj) {
        item.checkedNum += 1
      } else {
        item.checkedNum -= 1
      }
      switch (type) {
        case 'typeOne':
          if (item.checkedNum === 0) {
            item.check = false
          } else {
            item.check = true
          }
          break
        case 'typeTwo':
          if (item.checkedNum === this.checkedList[list][index].node.length) {
            item.check = true
          } else {
            item.check = false
          }
          break
      }
      this.allChecked(type, list, index, oneIndex, twoIndex)
    },
    // 全选排查
    allChecked(type, list, eq, oneIndex, twoIndex) {
      var item = this.checkedList[list][eq].node
      var isChecked = false
      switch (type) {
        case 'typeOne':
          if (twoIndex >= 0) {
            let secItem = item[oneIndex].node
            for (let y = 0; y < secItem.length; y++) {
              if (secItem[y].check) {
                isChecked = true
              }
            }
            this.checkedList[list][eq].node[oneIndex].check = isChecked
          }
          for (let x = 0; x < item.length; x += 1) {
            if (item[x].check) {
              isChecked = true
            }
          }
          break
        case 'typeTwo':
          isChecked = !item.some(item => {
            return item.check === false
          })
          break
      }
      this.checkedList[list][eq].check = isChecked
    },
    getSureList(list) {
      this.sureList = []
      // 遍历1级权限
      for (var i = 0; i < this.checkedList[list].length; i++) {
        var item1 = this.checkedList[list][i]
        if (item1.id && item1.check) {
          this.sureList.push(item1.id)
        }
        this.treeCallback(item1)
        // 遍历2级权限
        // if (item1.node) {
        //   for (var x = 0; x < item1.node.length; x++) {
        //     var item2 = item1.node[x]
        //     if (item2.id && item2.check) {
        //       this.sureList.push(item2.id)
        //     }
        //     // 遍历3级权限
        //     if (item2.node) {
        //       for (var y = 0; y < item2.node.length; y++) {
        //         var item3 = item2.node[y]
        //         if (item3.id && item3.check) {
        //           this.sureList.push(item3.id)
        //         }
        //       }
        //     }
        //   }
        // }
      }
      return this.sureList
    },
    treeCallback(obj) {
      if (obj.node) {
        for (let i = 0; i < obj.node.length; i++) {
          let item = obj.node[i]
          if (item.id && item.check) {
            this.sureList.push(item.id)
          }
          this.treeCallback(item)
        }
      }
    },
    getTableList() {
      return this.getSureList('tableList')
    }
  }
}
