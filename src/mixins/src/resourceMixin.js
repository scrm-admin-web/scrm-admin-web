import {
  mapGetters
} from 'vuex'

export default {
  computed: {
    ...mapGetters(['resourceActionMap', 'resourceMenuMap'])
  },
  methods: {
    /**
     * 是否拥有[resourceActionCode]的资源功能权限
     *
     * @param {*} resourceActionCode
     * @returns
     */
    hasResourceAction(resourceActionCode) {
      return !!this.resourceActionMap[resourceActionCode]
    },
    /**
     * 是否拥有[resourceActionCodeList]的资源功能权限中的任意一个权限
     *
     * @param {*} resourceActionCodeList
     * @returns
     */
    hasSomeResourceAction(resourceActionCodeList) {
      return resourceActionCodeList.some(resourceActionCode => this.hasResourceAction(resourceActionCode))
    },
    /**
     * 获取[resourceActionCode]的资源功能权限
     *
     * @param {*} resourceActionCode
     * @returns
     */
    getResourceAction(resourceActionCode) {
      return this.resourceActionMap[resourceActionCode]
    },
    /**
     * 是否拥有[resourceMenuUrl]的资源菜单权限
     *
     * @param {*} resourceMenuUrl
     * @returns
     */
    hasResourceMenu(resourceMenuUrl) {
      return !!this.resourceMenuMap[resourceMenuUrl]
    },
    /**
     * 获取[resourceMenuUrl]的资源菜单权限
     *
     * @param {*} resourceMenuUrl
     * @returns
     */
    getResourceMenu(resourceMenuUrl) {
      return this.resourceMenuMap[resourceMenuUrl]
    }
  }
}
