export default {
  data() {
    return {
      params: {
        current: 1,
        size: 10
      },
      date: '',
      tableData: {},
      formLabelWidth: '136px',
      createNotGetTableList: false,
      file: null
    }
  },
  computed: {
    // 是否显示分页，提取公共（逻辑可能发生变化）
    totalComputed() {
      let isShow = this.tableData.total > 0
      return isShow
    }
  },
  created() {
    if (!this.createNotGetTableList) {
      this.getTableList()
    }
  },
  methods: {
    async getTableList(pageNum, pageSize, type) {
      if (typeof pageNum === 'number') {
        this.params.current = pageNum
      }
      if (typeof pageSize === 'number') {
        this.params.size = pageSize
      }
      await this.requestTableList(type)
      if (this.tableData.total > 0 && this.tableData.current > 1 && this.tableData.records.length === 0) {
        this.getTableList(1)
      }
    },
    // 日期格式转换
    dateChange(val, begin, end) {
      // val在调用的时候，使用$event 代表第一个参数
      // https://blog.csdn.net/dangbai01_/article/details/83864498
      let beginTime = begin || 'beginTime'
      let endTime = end || 'endTime'
      if (!val) {
        this.params[beginTime] = ''
        this.params[endTime] = ''
      } else {
        this.params[beginTime] = new Date(val[0]).getTime()
        this.params[endTime] = new Date(val[1]).getTime()
      }
    },
    // 日期格式转换
    dateValChange(val) {
      let dateArr = val || []
      this.params.beginTime = dateArr[0] || ''
      this.params.endTime = dateArr[1] || ''
    },
    // 删除提示
    handleClose(id) {
      this.$confirm('是否确定删除？', '提示')
        .then(_ => {
          this.deleteCallback(id)
        })
        .catch(_ => {})
    },
    /**
     * 表格左侧选择--清除
     */
    clearSelection(refsName) {
      this.$refs[refsName].clearSelection()
    },
    // 上传文件
    fileChange: function (el) {
      el.preventDefault() // 取消默认行为
      this.file = el.target.files[0]
      el.target.value = null
      let arr = this.file.name.split('.')
      if (arr[1] !== 'xls' && arr[1] !== 'xlsx') {
        this.error('文件格式错误！')
      } else {
        let size = this.file.size // byte
        size = size / 1024 // kb
        size = (size / 1024).toFixed(3) // mb
        if (size > 10) {
          this.error('文件格式大小不能超过10M，当前文件大小为' + size + 'M！')
          return
        }
        let formData = new FormData()
        formData.append('file', this.file)
        this.fileUpload(formData)
      }
    }
  }
}
