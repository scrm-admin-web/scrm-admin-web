import * as types from '@/router/system/types'
import {
  walletApi
} from '@/api/system'
export default {
  data() {
    return {}
  },
  methods: {
    // 弹窗-打开
    openCallback(defObj, val, obj) {
      switch (val) {
        case 'see':
          this.addEditDialog = {
            open: true,
            title: '查看',
            isEdit: val,
            id: obj.id
          }
          break
        case 'add':
          this.walletTotal(val)
          break
        case 'edit':
          this.walletTotal(val, obj.id)
          break
        case 'statistics':
          this.statisticsDialog.id = obj.id
          this.statisticsDialog.open = true
          this.statisticsDialog.startTime = obj.startTime
          this.statisticsDialog.endTime = obj.endTime
          break
        case 'code':
          this.codeDialog = {
            open: true,
            id: null,
            url: obj.activityUrl ? obj.activityUrl : obj.linkQrCode
          }
          break
      }
    },
    // 获取余额
    async walletTotal(val, id) {
      let walletData = await walletApi.getAccountPrice()
      if (walletData.payId === 0) {
        if (walletData.leftMoney < 10 && walletData.leftMoney !== 0) {
          this.$confirm(`当前账户余额为${walletData.leftMoney}元，避免影响活动进行请充值！`, '提示', {
            confirmButtonText: '去充值',
            cancelButtonText: '取消',
            type: 'warning'
          }).then(_ => {
            this.$router.push({
              name: types.PACKETS_RECHARGE
            })
          }).catch(_ => {
            this.addEditDialogJudge(val, id)
          })
        } else if (walletData.leftMoney === 0) {
          this.$alert(`当前账户余额为${walletData.leftMoney}元，避免影响活动进行请充值！`, '提示', {
            confirmButtonText: '去充值',
            type: 'warning'
          }).then(_ => {
            this.$router.push({
              name: types.PACKETS_RECHARGE
            })
          }).catch(_ => {})
        } else {
          this.addEditDialogJudge(val, id)
        }
      } else {
        this.addEditDialogJudge(val, id)
      }
    },
    addEditDialogJudge(val, id) {
      switch (val) {
        case 'add':
          this.addEditDialog = {
            open: true,
            title: '新增',
            isEdit: val
          }
          break
        case 'edit':
          this.addEditDialog = {
            open: true,
            title: '编辑',
            isEdit: val,
            id: id
          }
          break
      }
    },
    // 审核-其他信息处理
    handleCallback(data) {
      if (data.records && data.records.length > 0) {
        data.records.forEach(element => {
          if (element.failReason) {
            element.failReason = element.failReason.split('|')
            element.failReason.forEach((item, index) => {
              element.failReason[index] = (index + 1) + '.' + item
            })
          }
          if (element.checkRemark) {
            element.checkRemark = element.checkRemark.split('\n')
          }
        })
      }
      return data
    }
  }
}
