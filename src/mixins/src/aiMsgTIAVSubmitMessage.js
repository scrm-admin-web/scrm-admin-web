export default {
  methods: {
    /**
     * 保存--消息的内容
     */
    submitMessage(info) {
      this.params.msgType = info.msgType
      // 小程序入口
      if (info.msgType === 6) {
        this.params.msgObject = info.msgObject
        // 小程序的标题即为列表的内容
        this.params.msgText = JSON.stringify(this.params.msgObject)
      } else if (info.msgType === 7) {
        this.params.msgObject = info.msgObject
        this.params.msgText = JSON.stringify(info.msgObject)
      } else {
        this.params.msgText = info.msgText
        this.params.coverInfo = info.coverInfo || ''
        this.params.voiceLength = info.voiceLength || ''
        // 小程序
        this.params.msgObject = info.msgObject
      }
      if (this.dialogProp.data.id) {
        // 修改
        this.submitModifyInfo()
      } else {
        // 新增
        this.submitAddInfo()
      }
    }
  }
}
