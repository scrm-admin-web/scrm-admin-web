import {
  mapGetters
} from 'vuex'
import {
  webIcon,
  webTitle
} from '@/config'

export default {
  computed: {
    ...mapGetters([
      'loginInfo'
    ])
  },
  watch: {
    'loginInfo'(value) {
      this.setHtmlTxt(value)
    }
  },
  mounted() {
    this.setHtmlTxt(this.loginInfo)
  },
  methods: {
    setHtmlTxt(obj) {
      if (!obj.useApiInfo) {
        return false
      }
      let link = document.querySelector("link[rel*='icon']") || document.createElement('link')
      link.type = 'image/x-icon'
      link.rel = 'shortcut icon'
      link.href = obj.icon || webIcon
      document.getElementsByTagName('head')[0].appendChild(link)
      document.title = obj.name || webTitle
    }
  }
}
