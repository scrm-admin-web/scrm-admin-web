import elementUIPlugin from './src/elementUI'
import vmaAssistPlugin from './src/vmaAssist'
import vmaElementPlugin from './src/vmaElement'
import axiosPlugin from './src/axios'

export default {
  install(Vue) {
    Vue.use(elementUIPlugin)
    Vue.use(vmaAssistPlugin)
    Vue.use(vmaElementPlugin)
    Vue.use(axiosPlugin)
  }
}
