import axios from 'axios'
import imAxios from '@/api/chat/imAxios'
import {
  isDev
} from '@/config'
import {
  macKeyCookie
} from '@/storage'

export default {
  install(Vue, opts) {
    // 开发环境下关闭认证
    // 当 env.NODE_ENV === 'dev' || env.NODE_ENV === 'development'时 请求头添加 macKey
    // if (isDev) {
    if (!isDev) {
      const authorizationInterceptor = config => {
        if (!config.headers.Authorization) {
          config.headers.Authorization = macKeyCookie.get() || ''
        }
        return config
      }

      axios.interceptors.request.use(authorizationInterceptor)
      imAxios.interceptors.request.use(authorizationInterceptor)
    }
  }
}
