const PREFIX = 'PMS'

// 框架页
export const MAIN = `${PREFIX}/MAIN`
// 首页
export const HOME = `${PREFIX}/HOME`
// 通知
export const NOTICE = `${PREFIX}/NOTICE`
// 企业列表
export const BUSINESS_LIST = `${PREFIX}/BUSINESS_LIST`
