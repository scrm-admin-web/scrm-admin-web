import Vue from 'vue'
import Router from 'vue-router'
// import uiRouter from './ui'
import systemRouter from './system/router'
import chatRouter from './chat/router'
import loginRouter from './login/router'
import commonRouter from './common/router'
import smsRouter from './sms/router'
import omsRouter from './oms/router'
import pmsRouter from './pms/router'
import * as smsTypes from './sms/types'
import * as omsTypes from './oms/types'
import * as pmsTypes from './pms/types'
import * as systemTypes from './system/types'

Vue.use(Router)

const frame = {
    path: '',
    component: resolve => require(['@/pages/main'], resolve),
    children: [{
        path: '/chat',
        component: resolve => require(['@/pages/chat/main'], resolve),
        children: chatRouter
    }, {
        path: '/system',
        name: systemTypes.MAIN,
        component: resolve => require(['@/pages/system/main'], resolve),
        children: systemRouter
    }, {
        path: '/sms',
        name: smsTypes.MAIN,
        component: resolve => require(['@/pages/sms/main'], resolve),
        children: smsRouter
    }, {
        path: '/oms',
        name: omsTypes.MAIN,
        component: resolve => require(['@/pages/oms/main'], resolve),
        children: omsRouter
    }, {
        path: '/pms',
        name: pmsTypes.MAIN,
        component: resolve => require(['@/pages/pms/main'], resolve),
        children: pmsRouter
    }]
}

// 整合ui页面路由
// frame.children.push({
//   path: '/ui',
//   component: resolve => require(['@/pages/ui/main'], resolve),
//   children: uiRouter
// })

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location)
}

const router = new Router({
    routes: [
        ...loginRouter,
        frame,
        ...commonRouter
    ]
})

// 是否为各个后台的框架页
export const isMainRouter = routerName => routerName && [smsTypes.MAIN, omsTypes.MAIN, pmsTypes.MAIN, systemTypes.MAIN].includes(routerName)

export default router