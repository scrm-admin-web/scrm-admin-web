import * as types from './types'
import {
  loginTypeLocal
} from '@/storage'
import router from '../router'
import loginRouter from './router'

// 是否为登录页的路由名称
export const isLoginRouter = routerName => Object.keys(types).some(key => types[key] === routerName)

// 重定向到系统后台登录页
export const redirectToSystemLogin = () => {
  loginTypeLocal.system()
  router.replace({
    name: types.LOGIN_SYSTEM
  })
}

// 重定向到员工后台登录页
export const redirectToChatLogin = () => {
  loginTypeLocal.chat()
  router.replace({
    // 员工后台与企业后台共用登录页
    name: types.LOGIN_SYSTEM
  })
}

// 重定向到运营商后台登录页
export const redirectToOmsLogin = () => {
  loginTypeLocal.oms()
  router.replace({
    name: types.LOGIN_OMS
  })
}

// 重定向到超级后台登录页
export const redirectToSmsLogin = () => {
  loginTypeLocal.sms()
  router.replace({
    name: types.LOGIN_SMS
  })
}

// 重定向到代理商后台登录页
export const redirectToPmsLogin = () => {
  loginTypeLocal.pms()
  router.replace({
    name: types.LOGIN_PMS
  })
}

export default loginRouter
