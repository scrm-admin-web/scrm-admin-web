import * as types from './types'
import router from '../router'
import {
  loginTypeLocal
} from '@/storage'
import chatRouter from './router'

// 是否为chat页面路由
export const isChatRouter = routerName => Object.keys(types).some(key => types[key] === routerName)

// 重定向到首页
export const redirectToChatHome = () => {
  loginTypeLocal.chat()
  router.replace({
    name: types.HOME
  })
}

export default chatRouter
