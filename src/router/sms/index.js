import * as types from './types'
import router from '../router'
import {
  loginTypeLocal
} from '@/storage'
import smsRouter from './router'

// 是否为超级后台的路由
export const isSmsRouter = routerName => Object.keys(types).some(key => types[key] === routerName)

// 重定向到首页
export const redirectToSmsHome = () => {
  loginTypeLocal.sms()
  router.replace({
    name: types.MAIN
  })
}

export default smsRouter
