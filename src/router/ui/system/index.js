import * as types from './types'

export default [{
  path: 'home',
  name: types.HOME,
  component: resolve =>
    require(['@/pages/ui/system/home/home.vue'], resolve)
}, {
  path: 'homev221',
  name: types.HOME_V221,
  component: resolve =>
    require(['@/pages/ui/system/homev2.2.1/home.vue'], resolve)
}, {
  path: 'notice',
  name: types.NOTICE,
  component: resolve =>
    require(['@/pages/ui/system/home/notice.vue'], resolve)
}, {
  path: 'noticeDetail',
  name: types.NOTICE_DETAIL,
  component: resolve =>
    require(['@/pages/ui/system/home/noticeDetail.vue'], resolve)
}, {
  path: 'sensitiveWordSet',
  name: types.SENSITIVE_WORD_SET,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/weChatWindControl/sensitiveWordSet.vue'], resolve)
}, {
  path: 'sensitiveOperation',
  name: types.SENSITIVE_OPERATION,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/weChatWindControl/sensitiveOperation.vue'], resolve)
}, {
  path: 'sensitiveNotice',
  name: types.SENSITIVE_NOTICE,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/weChatWindControl/sensitiveNotice.vue'], resolve)
}, {
  path: 'friendStatistics',
  name: types.FRIEND_STATISTICS,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/weChatWindControl/friendStatistics.vue'], resolve)
}, {
  path: 'groupStatistics',
  name: types.GROUP_STATISTICS,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/weChatWindControl/groupStatistics.vue'], resolve)
}, {
  path: 'friendExport',
  name: types.FRIEND_EXPORT,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/weChatWindControl/friendExport.vue'], resolve)
}, {
  path: 'financialStatistics',
  name: types.FINANCIAL_STATISTICS,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/weChatWindControl/financialStatistics.vue'], resolve)
}, {
  path: 'callRecording',
  name: types.CALL_RECORDING,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/phoneWindControl/callRecording.vue'], resolve)
}, {
  path: 'smsSensitiveWord',
  name: types.SMS_SENSITIVE_WORD,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/phoneWindControl/smsSensitiveWord.vue'], resolve)
}, {
  path: 'phoneSms',
  name: types.PHONE_SMS,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/phoneWindControl/phoneSms.vue'], resolve)
}, {
  path: 'sensitiveSms',
  name: types.SENSITIVE_SMS,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/phoneWindControl/sensitiveSms.vue'], resolve)
}, {
  path: 'phoneSensitiveNotice',
  name: types.PHONE_SENSITIVE_NOTICE,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/phoneWindControl/phoneSensitiveNotice.vue'], resolve)
}, {
  path: 'appSettings',
  name: types.APP_SETTINGS,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/phoneWindControl/appSettings.vue'], resolve)
}, {
  path: 'authorityManage',
  name: types.AUTHORITY_MANAGE,
  component: resolve =>
    require(['@/pages/ui/system/riskManagement/phoneWindControl/authorityManage.vue'], resolve)
}, {
  path: 'personNumList',
  name: types.PERSON_NUM_LIST,
  component: resolve =>
    require(['@/pages/ui/system/personalNumManage/personNumList.vue'], resolve)
}, {
  path: 'friendsList',
  name: types.FRIENDS_LIST,
  component: resolve =>
    require(['@/pages/ui/system/personalNumManage/friendsList.vue'], resolve)
}, {
  path: 'groupView',
  name: types.GROUP_VIEW,
  component: resolve =>
    require(['@/pages/ui/system/personalNumManage/groupView.vue'], resolve)
}, {
  path: 'groupManage',
  name: types.GROUP_MANAGE,
  component: resolve =>
    require(['@/pages/ui/system/personalNumManage/groupManage.vue'], resolve)
}, {
  path: 'distributionRecord',
  name: types.DISTRIBUTION_RECORD,
  component: resolve =>
    require(['@/pages/ui/system/personalNumManage/distributionRecord.vue'], resolve)
}, {
  path: 'deviceManage',
  name: types.DEVICE_MANAGE,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/deviceManage.vue'], resolve)
}, {
  path: 'deviceGroup',
  name: types.DEVICE_GROUP,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/deviceGroup.vue'], resolve)
}, {
  path: 'deviceDistribution',
  name: types.DEVICE_DISTRIBUTION,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/deviceDistribution.vue'], resolve)
}, {
  path: 'wechatSetting',
  name: types.WECHAT_SETTING,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/wechatSetting.vue'], resolve)
}, {
  path: 'quickReply',
  name: types.QUICK_REPLY,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/quickReply.vue'], resolve)
}, {
  path: 'subordinatesReply',
  name: types.SUBORDINATES_REPLY,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/subordinatesReply.vue'], resolve)
}, {
  path: 'autoFriends',
  name: types.AUTO_FRIENDS,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/autoFriends.vue'], resolve)
}, {
  path: 'globalReply',
  name: types.GLOBAL_REPLY,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/globalReply.vue'], resolve)
}, {
  path: 'personalNumAutoReply',
  name: types.PERSONAL_NUM_AUTO_REPLY,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/personalNumAutoReply.vue'], resolve)
}, {
  path: 'materialManage',
  name: types.MATERIAL_MANAGE,
  component: resolve =>
    require(['@/pages/ui/system/deviceManage/materialManage.vue'], resolve)
}, {
  path: 'packetsRecharge',
  name: types.PACKETS_RECHARGE,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/redPacketMarket/packetsRecharge.vue'], resolve)
}, {
  path: 'packetsRecord',
  name: types.PACKETS_RECORD,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/redPacketMarket/packetsRecord.vue'], resolve)
}, {
  path: 'showPicture',
  name: types.SHOW_PICTURE,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/showPicture/showPicture.vue'], resolve)
}, {
  path: 'activityAudit',
  name: types.ACTIVITY_AUDIT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/showPicture/activityAudit.vue'], resolve)
}, {
  path: 'activityRecord',
  name: types.ACTIVITY_RECORD,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/showPicture/activityRecord.vue'], resolve)
}, {
  path: 'smsRecharge',
  name: types.SMS_RECHARGE,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/smsMarket/smsRecharge.vue'], resolve)
}, {
  path: 'groupSend',
  name: types.GROUP_SEND,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/smsMarket/groupSend.vue'], resolve)
}, {
  path: 'historyTemplate',
  name: types.HISTORY_TEMPLATE,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/smsMarket/historyTemplate.vue'], resolve)
}, {
  path: 'sendRecord',
  name: types.SEND_RECORD,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/smsMarket/sendRecord.vue'], resolve)
}, {
  path: 'regularCircle',
  name: types.REGULAR_CIRCLE,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/regularCircle.vue'], resolve)
}, {
  path: 'microJump',
  name: types.MICRO_JUMP,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/microJump.vue'], resolve)
}, {
  path: 'autoPraise',
  name: types.AUTO_PRAISE,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/autoPraise.vue'], resolve)
}, {
  path: 'phonePlus',
  name: types.PHONE_PLUS,
  component: resolve =>
    require(['@/pages/ui/system/powderManage/phonePlus.vue'], resolve)
}, {
  path: 'timingPlus',
  name: types.TIMING_PLUS,
  component: resolve =>
    require(['@/pages/ui/system/powderManage/timingPlus.vue'], resolve)
}, {
  path: 'microCode',
  name: types.MICRO_CODE,
  component: resolve =>
    require(['@/pages/ui/system/powderManage/microCode.vue'], resolve)
}, {
  path: 'storePlus',
  name: types.STORES_PLUS,
  component: resolve =>
    require(['@/pages/ui/system/powderManage/storePlus.vue'], resolve)
}, {
  path: 'departmentManage',
  name: types.DEPARTMENT_MANAGE,
  component: resolve =>
    require(['@/pages/ui/system/staffManage/departmentManage.vue'], resolve)
}, {
  path: 'roleManage',
  name: types.ROLE_MANAGE,
  component: resolve =>
    require(['@/pages/ui/system/staffManage/roleManage.vue'], resolve)
}, {
  path: 'storeList',
  name: types.STORE_LIST,
  component: resolve =>
    require(['@/pages/ui/system/ecommercePlatform/storeList.vue'], resolve)
}, {
  path: 'storeOrder',
  name: types.STORE_ORDER,
  component: resolve =>
    require(['@/pages/ui/system/ecommercePlatform/storeOrder.vue'], resolve)
}, {
  path: 'operationLog',
  name: types.OPERATION_LOG,
  component: resolve =>
    require(['@/pages/ui/system/operationLog/operationLog.vue'], resolve)
}, {
  path: 'importOrder',
  name: types.IMPORT_ORDER,
  component: resolve =>
    require(['@/pages/ui/system/ecommercePlatform/importOrder.vue'], resolve)
}, {
  path: 'walletAccountSetting',
  name: types.ACCOUNT_SETTINGS,
  component: resolve =>
    require(['@/pages/ui/system/wallet/balanceAccount/accountSettings.vue'], resolve)
}, {
  path: 'mControlAccountBalance',
  name: types.M_CONTROL_ACCOUNT_BALANCE,
  component: resolve =>
    require(['@/pages/ui/system/wallet/balanceAccount/mControlAccountBalance.vue'], resolve)
}, {
  path: 'mCustomerRedBagRecord',
  name: types.M_CUSTOMER_RED_RECORD,
  component: resolve =>
    require(['@/pages/ui/system/wallet/balanceAccount/mCustomerRedBagRecord.vue'], resolve)
}, {
  path: 'walletSmsRecharge',
  name: types.WALLET_SMS_RECHARGE,
  component: resolve =>
    require(['@/pages/ui/system/wallet/balanceSms/walletSmsRecharge.vue'], resolve)
}, {
  path: 'dialogs',
  name: types.DIALOGS,
  component: resolve =>
    require(['@/pages/ui/system/dialogs/dialog.vue'], resolve)
}, {
  path: 'menu',
  name: types.MENU,
  component: resolve =>
    require(['@/pages/ui/system/menu/index.vue'], resolve)
}, {
  path: 'buyerShow',
  name: types.BUYER_SHOW,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/buyerShow/buyerShow.vue'], resolve)
}, {
  path: 'buyerShowaudit',
  name: types.BUYER_SHOW_AUDIT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/buyerShow/Audit.vue'], resolve)
}, {
  path: 'buyerShowoutlay',
  name: types.BUYER_SHOW_OUTLAY,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/buyerShow/Outlay.vue'], resolve)
}, {
  path: 'bindingGift',
  name: types.BINDING_GIFT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/bindingGift/bindingGift.vue'], resolve)
}, {
  path: 'bindingRecoring',
  name: types.BINDING_RECORDING,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/bindingGift/Recording.vue'], resolve)
}, {
  path: 'addCartGift',
  name: types.ADD_CART_GIFT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/addCartGift/addCartGift.vue'], resolve)
}, {
  path: 'addCartAudit',
  name: types.ADD_CART_AUDIT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/addCartGift/Audit.vue'], resolve)
}, {
  path: 'addCartOutlay',
  name: types.ADD_CART_OUTLAY,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/addCartGift/Outlay.vue'], resolve)
}, {
  path: 'addEvaOutlay',
  name: types.ADD_EVA_OUTLAY,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/addEvaGift/Outlay.vue'], resolve)
}, {
  path: 'addEvaAudit',
  name: types.ADD_EVA_AUDIT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/addEvaGift/Audit.vue'], resolve)
}, {
  path: 'addCartGift',
  name: types.ADD_EVA_GIFT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/addEvaGift/addEvaGift.vue'], resolve)
}, {
  path: 'collectionGift',
  name: types.COLLECTION_GIFT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/collectionGift/collectionGift.vue'], resolve)
}, {
  path: 'collectionAudit',
  name: types.COLLECTION_AUDIT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/collectionGift/Audit.vue'], resolve)
}, {
  path: 'collectionOutlay',
  name: types.COLLECTION_OUTLAY,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/collectionGift/Outlay.vue'], resolve)
}, {
  path: 'newVote',
  name: types.NEWVOTE,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/newVote/newVote.vue'], resolve)
}, {
  path: 'newVoteAudit',
  name: types.NEWVOTE_AUDIT,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/newVote/Audit.vue'], resolve)
}, {
  path: 'newVoteOutLay',
  name: types.NEWVOTE_OUTLAY,
  component: resolve =>
    require(['@/pages/ui/system/marketPlatform/newVote/Outlay.vue'], resolve)
}
]
