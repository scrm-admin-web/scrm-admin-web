const PREFIX = 'SYSTEM'

// 框架页
export const MAIN = `${PREFIX}/MAIN`
// 首页
export const HOME = `${PREFIX}/HOME`
// 数据中心
export const DATA_CENTER = `${PREFIX}/DATA_CENTER`
// 通知
export const NOTICE = `${PREFIX}/NOTICE`
// 员工通知
export const NOTICESTAFF = `${PREFIX}/NOTICESTAFF`
// 设备管理-设备管理
export const DEVICE_MANAGE = `${PREFIX}/DEVICE_MANAGEMENT`
// 入库管理
export const WAREHOUSE_MANAGE = `${PREFIX}/WAREHOUSE_MANAGEMENT`
// 设备分组
export const DEVICE_GROUP = `${PREFIX}/DEVICE_GROUP`
// 分配记录
export const DEVICE_DISTRIBUTION = `${PREFIX}/DEVICE_DISTRIBUTION`
// 微信设置
export const WECHAT_SETTING = `${PREFIX}/WECHAT_SETTING`
// 快捷回复
export const QUICK_REPLY = `${PREFIX}/QUICK_REPLY`
// 下属快捷回复
export const SUBORDINATES_REPLY = `${PREFIX}/SUBORDINATES_REPLY`
// 自动通过好友
export const AUTO_FRIENDS = `${PREFIX}/AUTO_FRIENDS`
// 全局自动回复
export const GLOBAL_REPLY = `${PREFIX}/GLOBAL_REPLY`
// 个人号自动回复
export const PERSONAL_NUM_AUTO_REPLY = `${PREFIX}/PERSONAL_NUM_AUTO_REPLY`
// 素材管理
export const MATERIAL_MANAGE = `${PREFIX}/MATERIAL_MANAGE`
// 定时发圈
export const REGULAR_CIRCLE = `${PREFIX}/REGULAR_CIRCLE`
// 电商平台-店面列表
export const STORE_LIST = `${PREFIX}/STORE_LIST`
// 店面订单
export const STORE_ORDER = `${PREFIX}/STORE_ORDER`
// 导入订单
export const IMPORT_ORDER = `${PREFIX}/IMPORT_ORDER`
// 朋友圈发圈记录
export const RECORD_CIRCLE = `${PREFIX}/RECORD_CIRCLE`
// 聊天消息质检
export const MESSAGE_CHECK = `${PREFIX}/MESSAGE_CHECK`
// 微跳转
export const MICRO_JUMP = `${PREFIX}/MICRO_JUMP`
// 朋友圈自动点赞
export const AUTO_PRAISE = `${PREFIX}/AUTO_PRAISE`
// 红包充值
export const PACKETS_RECHARGE = `${PREFIX}/RED_PACKET_MARKET/PACKETS_RECHARGE`
// 红包记录
export const PACKETS_RECORD = `${PREFIX}/RED_PACKET_MARKET/PACKETS_RECORD`
// 短信充值
export const SMS_RECHARGE = `${PREFIX}/SMS_RECHARGE`
// 晒图活动
export const SHOW_PICTURE = `${PREFIX}/SHOW_PICTURE`
// 手机H5展示
export const H5_SHOW = `${PREFIX}/H5_SHOW`
// 活动审核
export const ACTIVITY_AUDIT = `${PREFIX}/ACTIVITY_AUDIT`
// 活动支出记录
export const ACTIVITY_RECORD = `${PREFIX}/ACTIVITY_RECORD`
// 短信群发
export const GROUP_SEND = `${PREFIX}/GROUP_SEND`
// 短信历史模板
export const HISTORY_TEMPLATE = `${PREFIX}/HISTORY_TEMPLATE`
// 短信发送记录
export const SEND_RECORD = `${PREFIX}/SEND_RECORD`
// 操作日志
export const OPERATION_LOG = `${PREFIX}/OPERATION_LOG`
// 个人号管理-个人号列表
export const PERSON_NUM_LIST = `${PREFIX}/PERSON_NUM_LIST`
// 微信好友列表
export const FRIENDS_LIST = `${PREFIX}/FRIENDS_LIST`
// 微信群查看
export const GROUP_VIEW = `${PREFIX}/GROUP_VIEW`
// 群发记录
export const GROUP_RECORDS = `${PREFIX}/GROUP_RECORDS`
// 分组管理
export const GROUP_MANAGE = `${PREFIX}/GROUP_MANAGE`
// 分配记录
export const DISTRIBUTION_RECORD = `${PREFIX}/DISTRIBUTION_RECORD`
// 加粉管理-手机号加好友
export const PHONE_PLUS = `${PREFIX}/PHONE_PLUS_FRIEND`
// 加粉管理-定时加好友
export const TIMING_PLUS = `${PREFIX}/TIMING_PLUS_FRIEND`
// 加粉管理-微活码
export const MICRO_CODE = `${PREFIX}/MICRO_CODE`
// 加粉管理-店铺自动加好友
export const STORES_PLUS = `${PREFIX}/STORES_PLUS`
// 风控管理-微信风控-敏感词设置
export const SENSITIVE_WORD_SET = `${PREFIX}/SENSITIVE_WORD_SET`
// 风控管理-微信风控-敏感词分组
export const SENSITIVE_WORD_GROUP = `${PREFIX}/SENSITIVE_WORD_GROUP`
// 风控管理-微信风控-敏感操作
export const SENSITIVE_OPERATION = `${PREFIX}/SENSITIVE_OPERATION`
// 风控管理-微信风控-敏感通知
export const SENSITIVE_NOTICE = `${PREFIX}/SENSITIVE_NOTICE`
// 风控管理-微信风控-重复好友统计
export const FRIEND_STATISTICS = `${PREFIX}/FRIEND_STATISTICS`
// 风控管理-微信风控-重复群统计
export const GROUP_STATISTICS = `${PREFIX}/GROUP_STATISTICS`
// 风控管理-微信风控-微信好友导出
export const FRIEND_EXPORT = `${PREFIX}/FRIEND_EXPORT`
// 风控管理-微信风控-微信财务统计
export const FINANCIAL_STATISTICS = `${PREFIX}/FINANCIAL_STATISTICS`
// 风控管理-手机风控-通话录音
export const CALL_RECORDING = `${PREFIX}/CALL_RECORDING`
// 风控管理-手机风控-短信敏感词
export const SMS_SENSITIVE_WORD = `${PREFIX}/SMS_SENSITIVE_WORD`
// 风控管理-手机风控-手机短信
export const PHONE_SMS = `${PREFIX}/PHONE_SMS`
// 风控管理-手机风控-敏感短信
export const SENSITIVE_SMS = `${PREFIX}/SENSITIVE_SMS`
// 风控管理-手机风控-敏感通知
export const PHONE_SENSITIVE_NOTICE = `${PREFIX}/PHONE_SENSITIVE_NOTICE`
// 风控管理-手机风控-APP设置
export const APP_SETTINGS = `${PREFIX}/APP_SETTINGS`
// 风控管理-手机风控-权限管理
export const AUTHORITY_MANAGE = `${PREFIX}/AUTHORITY_MANAGE`
// 员工管理-部门管理
export const DEPARTMENT_MANAGE = `${PREFIX}/DEPARTMENT_MANAGE`
// 员工管理-角色管理
export const ROLE_MANAGE = `${PREFIX}/ROLE_MANAGE`
// 营销平台-首绑有礼
export const FIRST_BIND_LIST = `${PREFIX}/FIRST_BIND_LIST`
// 营销平台-首绑有礼-绑定记录
export const FIRST_BIND_RECORD = `${PREFIX}/FIRST_BIND_RECORD`
// 营销平台-追评有礼
export const REVIEW_LIST = `${PREFIX}/REVIEW_LIST`
// 营销平台-追评有礼-活动审核
export const REVIEW_ACTIVITY_AUDIT = `${PREFIX}/REVIEW_ACTIVITY_AUDIT`
// 营销平台-追评有礼-支出记录
export const REVIEW_RECORD = `${PREFIX}/REVIEW_RECORD`
// 营销平台-加购有礼
export const ADD_PURCHASE_LIST = `${PREFIX}/ADD_PURCHASE_LIST`
// 营销平台-加购有礼-活动审核
export const ADD_ACTIVITY_AUDIT = `${PREFIX}/ADD_ACTIVITY_AUDIT`
// 营销平台-加购有礼-支出记录
export const ADD_PURCHASE_RECORD = `${PREFIX}/ADD_PURCHASE_RECORD`
// 营销平台-收藏有礼
export const COLLECTION_LIST = `${PREFIX}/COLLECTION_LIST`
// 营销平台-收藏有礼-活动审核
export const COLLECTION_ACTIVITY_AUDIT = `${PREFIX}/COLLECTION_ACTIVITY_AUDIT`
// 营销平台-收藏有礼-支出记录
export const COLLECTION_RECORD = `${PREFIX}/COLLECTION_RECORD`
// 营销平台-新品投票
export const NEW_VOTING_LIST = `${PREFIX}/NEW_VOTING_LIST`
// 营销平台-新品投票-活动审核
export const NEW_VOTING_ACTIVITY_AUDIT = `${PREFIX}/NEW_VOTING_ACTIVITY_AUDIT`
// 营销平台-新品投票-支出记录
export const NEW_VOTING_RECORD = `${PREFIX}/NEW_VOTING_RECORD`
// 营销平台-买家秀
export const BUYING_SHOW_LIST = `${PREFIX}/BUYING_SHOW_LIST`
// 营销平台-买家秀-活动审核
export const BUYING_ACTIVITY_AUDIT = `${PREFIX}/BUYING_ACTIVITY_AUDIT`
// 营销平台-买家秀-支出记录
export const BUYING_SHOW_RECORD = `${PREFIX}/BUYING_SHOW_RECORD`
// 钱包--账户余额--账户设置
export const ACCOUNT_SETTINGS = `${PREFIX}/ACCOUNT_SETTINGS`
// 钱包--账户余额--微码微控账户余额
export const M_CONTROL_ACCOUNT_BALANCE = `${PREFIX}/M_CONTROL_ACCOUNT_BALANCE`
// 钱包--账户余额--微客服代发红包记录
export const M_CUSTOMER_RED_RECORD = `${PREFIX}/M_CUSTOMER_RED_RECORD`
// 钱包--短信余额--短信充值
export const WALLET_SMS_RECHARGE = `${PREFIX}/WALLET_SMS_RECHARGE`
// 社群管理--引流--自动通过
export const AUTOMATIC_PASSING = `${PREFIX}/AUTOMATIC_PASSING`
// 社群管理--引流--自动回复
export const AUTOMATIC_RECOVERY = `${PREFIX}/AUTOMATIC_RECOVERY`
// 社群管理--引流--入群问候
export const GREETING_GROUP = `${PREFIX}/GREETING_GROUP`
// 社群管理--引流--群活动
export const GROUP_ACTIVITY = `${PREFIX}/GROUP_ACTIVITY`
// 社群管理--管理--群公告
export const ANNOUNCEMENT = `${PREFIX}/ANNOUNCEMENT`
// 社群管理--管理--行为管理
export const BEHAVIOR_MANAGE = `${PREFIX}/BEHAVIOR_MANAGE`
// 社群管理--管理--群管理
export const GROUPS_MANAGE = `${PREFIX}/GROUPS_MANAGE`
// 社群管理--统计--群统计
export const GROUPS_STATISTICS = `${PREFIX}/GROUPS_STATISTICS`
// 社群管理--统计--互动统计
export const INTERACTIVE_STATISTICS = `${PREFIX}/INTERACTIVE_STATISTICS`
// 社群管理--统计--单群统计
export const SINGLE_GROUP_STATISTICS = `${PREFIX}/SINGLE_GROUP_STATISTICS`
// 其他-系统配置
export const SYSTEM_CONFIG = `${PREFIX}/SYSTEM_CONFIG`
