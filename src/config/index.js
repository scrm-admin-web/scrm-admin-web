import {
  cipher
} from '@/utils'
import defautWechatAvtor from '@/assets/images/common/wechat-avtor.png'
import defaultCustomerServiceAvtor from '@/assets/images/common/customerService-avtor.png'
import defaultNoImg from '@/assets/images/common/noimg.png'
import defaultProductImg from '@/assets/images/common/product-avtor.png'

const env = cipher.decodeJSON(process.config)
const IS_DEV = env.NODE_ENV === 'dev' || env.NODE_ENV === 'development'
const IS_TEST = env.NODE_ENV === 'test' || env.NODE_ENV === 'testing'

export const nodeEnv = env.NODE_ENV
export const isDev = IS_DEV
export const isTest = IS_TEST
export const appNamespace = `${nodeEnv}/${env.app}`
export const api = env.api
export const imApi = env.imApi
export const aiApi = env.aiApi
export const aiWs = env.aiWs
export const cxsijp = env.cxsijp
export const aseKey = env.aseKey
// export const api = 'https://wechat.h5h5h5.cn'
// export const imApi = 'https://scrm.tencent.h5h5h5.cn'
export const getQiniuTokenUrl = type => `${api}/common/v1.0/img/qiniutoken`
export const ckeditorUploadUrl = `${api}/common/v1.0/img/editor`
const defautWechatAvtorUrl = 'https://keji-res.h5h5h5.cn/FjwLG7wbHScdQ1_DQ5V_uQImViWm?attname=blob.png'
export {
  // 默认的微信头像
  defautWechatAvtor,
  // 默认的微信头像网络地址，用于发红包时没有头像的情况
  defautWechatAvtorUrl,
  // 默认的客服头像
  defaultCustomerServiceAvtor,
  // 默认无图时显示的图片
  defaultNoImg,
  // 默认的商品图片
  defaultProductImg
}
// 微客服聊天服务器的im账号id
export const chatServerImAccountId = 'admin'
// vma-h5-scrm域名
export const vmaH5Scrm = env.vmaH5Scrm
// h5晒图领红包地址
export const vmaH5ScrmBlueprintRedPacket = `${env.vmaH5Scrm}/red_packet/blueprint`
// h5微信红包地址
export const vmaH5ScrmWechatRedPacket = `${env.vmaH5Scrm}/red_packet/wechat`
// h5微活码地址
export const vmaH5ScrmMicroCode = `${env.vmaH5Scrm}/micro_code`
// h5首绑有礼
export const vmaH5ScrmFirstBindGood = `${env.vmaH5Scrm}/first_bind_good/home`
// h5追评有礼
export const vmaH5ScrmAddEvaluateGood = `${env.vmaH5Scrm}/add_evaluate_good/home`
// h5加购有礼
export const vmaH5ScrmAddShopGood = `${env.vmaH5Scrm}/add_shop_good/home`
// h5收藏有礼
export const vmaH5ScrmcollectGood = `${env.vmaH5Scrm}/collect_good/home`
// h5新品投票
export const vmaH5ScrmnewVote = `${env.vmaH5Scrm}/new_vote/home`
// h5买家秀
export const vmaH5ScrmbuyersShow = `${env.vmaH5Scrm}/buyers_show/home`
// h5群活动地址
export const vmaH5ScrmGroupActivity = `${env.vmaH5Scrm}/group/activity`
// 登录页相关默认信息
export const loginInfo = {
  // 登陆页背景图
  bg: env.loginBg,
  // 登录页logo
  logo: env.loginLogo,
  // 客服热线
  hotLine: env.hotLine,
  // 备案号
  caseNumber: env.caseNumber,
  // 版权所有
  copyRight: env.copyRight,
  // 公众号二维码
  qrCode: env.qrCode,
  // 技术支持
  technicalSupport: env.technicalSupport
}
// tab上显示的默认小图标
export const webIcon = env.webIcon
// tab上显示的默认标题
export const webTitle = env.webTitle

// ************** 主题相关 **************
// 主题名
export const theme = env.theme
// 登录页logo
export const loginLogo = env.loginLogo
// 登陆页背景图
export const loginBg = env.loginBg
// 系统后台logo
export const systemLogo = env.systemLogo

// 是否显示微客服上商品推荐
export const showChatHeaderProductionRecommand = env.showChatHeaderProductionRecommand !== 'false'
// 是否显示微客服上购物订单
export const showChatHeaderShoppingOrder = env.showChatHeaderShoppingOrder !== 'false'
// 是否显示登录也客服热线
export const showLoginHotLine = env.showLoginHotLine !== 'false'
