// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// 加载样式主题
import './assets/css/theme'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import {
  eachOwn
} from '@/utils'
import * as componentLibs from './components/libs'
import * as filters from './filters'
import * as directives from './directives'
import plugins from './plugins'
import Logger from '@/utils/logger'
// 防止xss攻击
import xss from 'xss'
// 兼容IE11的插件
import 'babel-polyfill'
import 'es6-promise/auto'

Object.defineProperty(Vue.prototype, '$xss', {
  value: xss
})
// import BaiduMap from 'vue-baidu-map'

// import axios from 'axios'

// var timestamp=new Date().getTime()

// axios.interceptors.request.use(config => {
//   if(config["params"]){ //如果有params参数 以数组方式为对象添加值
//      config.params["token"]=timestamp
//   }else{
//     config["params"]={token:timestamp} //如果没有params参数 则增加一个params参数
//   }
//   // 一定要返回 config
//   return config
// })

Vue.config.productionTip = false

// 注册全局组件
eachOwn(componentLibs, (component, name) => {
  Vue.component(name, component)
})

// 注册全局过滤器
eachOwn(filters, (filter, name) => {
  Vue.filter(name, filter)
})

// 注册全局指令
eachOwn(directives, (directive, name) => {
  Vue.directive(name, directive)
})

// 安装全局插件
Vue.use(plugins)
/*
Vue.use(BaiduMap, {
  ak: 'gsRDxPxnqggmNxbglDITFebKy3wAor9S'
})
*/

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
Logger.info('初始化完成')
