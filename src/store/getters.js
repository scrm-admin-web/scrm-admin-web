export const demo = state => state.demo
export const navTagArr = state => state.navTagArr
export const leftMenuArr = state => state.leftMenuArr
export const tagList = state => state.tagList
export const theme = state => state.theme
export const adminInfo = state => state.adminInfo || {}
export const loginInfo = state => state.loginInfo || {}
export const chatIM = state => state.chatIM || {}
export const activeChatAccountInfo = state => state.activeChatAccountInfo || null
export const activeChatFriendInfo = state => state.activeChatFriendInfo || null
export const chatFooterSwitchs = state => state.chatFooterSwitchs || {}
export const chatSensitiveWords = state => state.chatSensitiveWords || {}
export const chatUserInfoDB = state => state.chatUserInfoDB
export const noticeTip = state => state.noticeTip
// 菜单映射
export const resourceMenuMap = state => state.resourceMenuMap || {}

// 资源功能权限映射
export const resourceActionMap = state => {
  const map = {}
  if (state.adminInfo && state.adminInfo.resourceActionList && state.adminInfo.resourceActionList.length) {
    state.adminInfo.resourceActionList.forEach(v => {
      map[v.code] = v
    })
  }
  return map
}
